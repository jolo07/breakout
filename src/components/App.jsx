import Footer from "./Footer";
import Header from "./Header";
const App = ({ children }) => (
  <main style={{ position: "relative" }}>
    <Header />
    {children}
    <Footer />
  </main>
);

export default App;
