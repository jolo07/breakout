import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import Icons from "../Icons";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import {
  Button,
  ProgramDetailsContainer,
  ProgramLanguage,
  MentorDescription,
  MentorTitleContainer,
  MentorName,
  MentorTitle,
  MentorDetailsContainer,
  BackgroundImageContainer,
  MentorDataContainer,
  ProgramContainer,
  ProgramDate,
  ProgramTitleContainer,
  ProgramTitle,
  ProgramPriceContainer,
  ProgramPrice,
  ProgramDuration,
  ProgramDescription,
  ProgramDescriptionContainer,
  MentorContactContainer,
  MentorContactDescription,
  SeatStockDetails,
  ProgramStatusContainer,
  EnrollmentDetails,
  GradientContainer,
  MentorHighlight,
} from "./style";
import { tracker } from "../../utils/tracker";
import { getProgramData } from "../../services/wooCommerce";
import { roundOffToNearestFifty } from "../../utils/helper";

Date.prototype.toShortFormat = function () {
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  let day = this.getDate();

  let monthIndex = this.getMonth();
  let monthName = monthNames[monthIndex];

  let year = this.getFullYear();

  return `${day} ${monthName} ${year}`;
};

const ConsultationCardComponent = ({ programs, type, mockData }) => {
  const [smallDevice, setSmallDevice] = useState(false);
  const [outOfStock, setOutOfStock] = useState(false);
  const [availableSeats, setAvailableSeats] = useState();
  const [programDate, setProgramDate] = useState();
  const [mentorData, setMentorData] = useState(mockData.mentorData);
  const [programData, setProgramData] = useState(mockData.programData);
  const [programPrice, setProgramPrice] = useState();

  useEffect(async () => {
    if (window.innerWidth < 768) {
      setSmallDevice(true);
    } else {
      setSmallDevice(false);
    }
  }, [programs]);

  return (
    <ProgramContainer type={type}>
      <MentorDataContainer>
        <GradientContainer />
        <BackgroundImageContainer>
          {smallDevice ? (
            <Image
              loader={gumletLoaderMarketfeedUniversity}
              src={mentorData.backgroundImage}
              width={266}
              height={200}
              objectFit="cover"
              alt="learn stock market trading"
            />
          ) : (
            <Image
              className="bcgImage"
              loader={gumletLoaderMarketfeedUniversity}
              src={mentorData.backgroundImage}
              width={400}
              height={400}
              objectFit="cover"
              alt={"learn stock market trading"}
            />
          )}
        </BackgroundImageContainer>
        <MentorDetailsContainer>
          <div className="mentorTitleContainer">
            <div className="mentorTitle">
              {mentorData?.mentorTitle ? mentorData?.mentorTitle : ""}
            </div>
            <div className="mentorName">
              {mentorData?.mentorName ? mentorData?.mentorName : ""}
            </div>
          </div>
          <div className="mentorDescription">
            {mentorData?.mentorDescription ? mentorData?.mentorDescription : ""}
          </div>
        </MentorDetailsContainer>
        <ProgramLanguage>
          {programData?.programLanguage ? (
            <div>{programData?.programLanguage}</div>
          ) : (
            ""
          )}
        </ProgramLanguage>
      </MentorDataContainer>
      <ProgramDetailsContainer>
        <ProgramTitleContainer>
          <ProgramTitle>
            {programData?.programTitle ? programData?.programTitle : ""}
          </ProgramTitle>
          <ProgramPriceContainer>
            <ProgramPrice>
              <span style={{ fontSize: 12 }}>₹</span>
              {mentorData?.mentorContactPrice}
            </ProgramPrice>
            <ProgramDuration>for 30mins</ProgramDuration>
          </ProgramPriceContainer>
        </ProgramTitleContainer>
        <ProgramDescriptionContainer>
          {programData.programDescription.map((point) => {
            return (
              <span>
                <ProgramDescription>
                  <span className="icon">
                    <Icons name={"bulletPoint"} />
                  </span>
                  <p>{point}</p>
                </ProgramDescription>
              </span>
            );
          })}
        </ProgramDescriptionContainer>
      </ProgramDetailsContainer>
      <ProgramStatusContainer>
        <EnrollmentDetails>
          <Icons name="experienceIcon" />
          &nbsp;
          <span className="value">{programData?.experience}+ Years </span>of
          Experience
        </EnrollmentDetails>
        <MentorHighlight>
          <Icons name={`${programData.highlightIcon}`} />
          &nbsp;
          <span className="value">{programData?.mentorHighlight?.value}</span>
          {programData?.mentorHighlight?.text}
        </MentorHighlight>
      </ProgramStatusContainer>
      <div className="buttonWrapper">
        <Link href={mentorData.mentorContactUrl}>
          <Button type={availableSeats <= 0 ? "soldOut" : "inStock"}>
            {availableSeats <= 0 ? "Learn More" : "Book Consultation"}
          </Button>
        </Link>
      </div>
    </ProgramContainer>
  );
};

export default ConsultationCardComponent;
