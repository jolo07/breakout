import React, { useEffect, useState } from "react";
import {
  Container,
  CouponDetailsContainer,
  CouponCodeContainer,
  Separator,
  Circle,
} from "./style";
const CouponLayout = ({
  discountPercentage = "50",
  courseName = "Stock Market Basics",
  currentPrice = "5999",
  discountedPrice = "2999",
  couponCode
}) => {
  return (
    <>
      <Container>
        <CouponDetailsContainer>
          <div className="couponDiscountContainer">
            {discountPercentage}% OFF
          </div>
          <div className="couponInfoContainer">
            <div className="couponTitle">{courseName}</div>
            <div className="couponProductPricing">
              <span className="couponProductDiscountedPrice">
                <span className="rupeeSymbol">₹</span>
                {currentPrice}
              </span>
              <span className="couponProductPrice">
                <span className="rupeeSymbol"> ₹</span>
                {discountedPrice}
                <span className="rupeeSymbol">/seat</span>
              </span>
            </div>
          </div>
        </CouponDetailsContainer>
        <CouponCodeContainer>
          <Circle top />
          <Circle bottom />
          <Separator />
          <div className="couponOuterContainer">
            <div className="couponInnerContainer">{couponCode}</div>
          </div>
        </CouponCodeContainer>
      </Container>
    </>
  );
};

export default CouponLayout;
