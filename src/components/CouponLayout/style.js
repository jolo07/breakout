import styled from "styled-components";
import { rem } from "../../utils/remConverter";
export const Container = styled.div`
  align-items: center;
  background-image: linear-gradient(to top, #fcf8e7, #fff6f0);
  border-radius: 5px;
  display: flex;
  justify-content: space-between;
  padding: 15px 20px;
  position: relative;
  @media (max-width: 767px) {
    padding: 10px;
  }
`;

export const CouponDetailsContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  gap: 20px;
  .couponDiscountContainer {
    color: #a253d2;
    font-family: gilroyextrabold;
    font-size: 24px;

    @media (max-width: 767px) {
      font-size: 12px;
    }
  }
  .couponProductPricing {
    color: #2c88bc;
    font-family: gilroyBold;
    text-align: left;
    .couponProductDiscountedPrice {
      font-size: 12px;
      text-decoration: line-through;
      @media (max-width: 767px) {
        font-size: 8px;
        margin-top: -10px;
      }
    }
    .couponProductPrice {
      font-size: 14px;
      @media (max-width: 767px) {
        font-size: 10px;
      }
    }
    .rupeeSymbol {
      font-size: 10px;
      @media (max-width: 767px) {
        font-size: 8px;
      }
    }
    @media (max-width: 767px) {
      margin-top: -5px;
    }
  }
  .couponTitle {
    color: #2c4cbc;
    font-family: gilroybold;
    font-size: 15px;
    @media (max-width: 767px) {
      font-family: gilroymedium;
      font-size: 10px;
    }
  }
  @media (max-width: 767px) {
    align-items: flex-start;
    flex-direction: column;
    gap: 0px;
  }
`;

export const CouponCodeContainer = styled.div`
position:relative;
  .couponOuterContainer {
    background-image: linear-gradient(to top, #5272e4, #b462e5);
    border-radius: 6px;
    color: #fff;
    padding: 2px;
    .couponInnerContainer {
      border: 1px dotted #fff;
      border-radius: 6px;
      font-family: gilroyextrabold;
      font-size: 16px;
      padding: 8px;
    }
  }
`;

export const Circle = styled.div`
  position: absolute;
  top: ${(props) => (props.top ? "-22px" : "50px")};
  left: -20px;
  height: 15px;
  width: 15px;
  border-radius: 50%;
  background-color: #fff;
  z-index: 1005;
  @media (max-width: 767px) {
    top: ${(props) => (props.top ? "-18px" : "46px")};
    left: -20px;
  }
`;
export const Separator = styled.div`
  position: absolute;
  top: ${(props) => (props.top ? "40px" : "-10px")};
  left: -14px;
  width: 1px;
  height: 60px;
  border: 1px dashed #d4d1c0;

  @media (max-width: 767px) {
    left: -13px;
  }
`;
