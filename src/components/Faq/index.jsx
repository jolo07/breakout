import { useEffect, useState } from "react";
import { Section, Wrapper, H4, Button } from "./style";
import Icons from "../Icons";
const Faq = () => {
  const [activeIndex, setActiveIndex] = useState();
  useEffect(() => {
    let toggles = document.getElementsByClassName("toggle");
    let contentDiv = document.getElementsByClassName("content");

    for (let i = 0; i < toggles.length; i++) {
      toggles[i].addEventListener("click", () => {
        if (
          parseInt(contentDiv[i].style.height) != contentDiv[i].scrollHeight
        ) {
          if (i === activeIndex) setActiveIndex(null);
          else setActiveIndex(i);
          contentDiv[i].style.height = contentDiv[i].scrollHeight + "px";
          toggles[i].style.color = "#3849a6";
        } else {
          contentDiv[i].style.height = "0px";
          toggles[i].style.color = "#484848";
          setActiveIndex(null);
        }

        for (let j = 0; j < contentDiv.length; j++) {
          if (j !== i) {
            contentDiv[j].style.height = 0;
            toggles[j].style.color = "#484848";
            toggles[j].name = "minus";
          }
        }
      });
    }
  }, []);
  return (
    <Section>
      <div className="container">
        <H4>Frequently Asked Questions</H4>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">
              How do I trust you? Are you even actually making profits? Isn’t
              this all a scam?
            </Button>
            <Icons
              className="toggle"
              name={0 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              We completely understand your feelings. Even we had the same
              questions in mind when we started out and saw many “stock market
              gurus” in our journey. Please make sure that you ask for and see
              the Profit and Loss report of whoever you are learning trading
              from. With that being said, we will be showing our P&L statements
              when you join the program. Any profitable market participant can
              tell you that it is indeed possible.
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">
              If you are successful in trading, then why do you need to sell
              courses for money?
            </Button>
            <Icons
              className="toggle"
              name={1 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              Another question we also asked many trainers ourselves. We are
              successful in trading, and we happen to be good teachers as well.
              ALL of our basic educational content is still FREE, but we charge
              for giving you our time and the opportunity to become successful
              yourself. And if we are making some money in the process, it
              doesn’t hurt, right?
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">What is marketfeed?</Button>
            <Icons
              className="toggle"
              name={2 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              marketfeed is an online educational platform for stock market
              trading, currently maintained on Discord. The pre-recorded videos
              are hosted on YouTube and daily live sessions are conducted on
              Zoom. We teach you how to navigate the stock market, learn as a
              community, trade, and earn!
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">How do I join the marketfeed?</Button>
            <Icons
              className="toggle"
              name={3 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              All you have to do is select the course that you are interested in
              and register online by clicking here. If you are unsure, just talk
              to our support team. We will help you out.
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">
              Will I get a certificate for completing the stock market trading
              course?
            </Button>
            <Icons
              className="toggle"
              name={4 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              Absolutely! After all the hard work, you deserve something to show
              for it.
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">
              How exactly will your stock market trading courses help me start
              making money?
            </Button>
            <Icons
              className="toggle"
              name={5 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              We will teach you proven strategies, tips, and tricks used by us
              so you can make consistent profits from the market, just like us.
              With live trading sessions, daily Q&As and more we have created a
              community that grows as a whole. Join us and we’ll make sure we
              all win together!
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">What’s in it for me?</Button>
            <Icons
              className="toggle"
              name={6 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              Your success. It’s that simple. Right from the start to you
              succeeding in the market, we will be with you in every step.
              <br /> Our online stock trading courses are designed to
              democratize the stock market and help you attain financial
              freedom. Making a positive difference in the lives of people and
              helping them improve their quality of life is enough for us.
            </p>
          </div>
        </Wrapper>
        <Wrapper>
          <div className="toggleWrapper">
            <Button className="toggle">
              Can I get a refund if I cannot join the program?
            </Button>
            <Icons
              className="toggle"
              name={7 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              You cannot get a refund once the payment is completed. We can
              shift you to a different batch, as per your convenience. For
              further doubts, you can refer to our complete refund policy.
            </p>
          </div>
        </Wrapper>
        <Wrapper last={true}>
          <div className="toggleWrapper">
            <Button className="toggle">
              Do you plan to introduce more courses?
            </Button>
            <Icons
              className="toggle"
              name={7 === activeIndex ? "minus" : "plus"}
            />
          </div>
          <div className="content">
            <p>
              We do plan to bring in more expert traders so that you can learn
              stock market trading from the perspectives of different
              individuals. New announcements will be made soon.
            </p>
          </div>
        </Wrapper>
      </div>
    </Section>
  );
};

export default Faq;
