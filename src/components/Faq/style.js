import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Section = styled.div`
  display: grid;
  place-items: center;
  background: linear-gradient(158.73deg, #fffcf1 -0.08%, #eefeff 102.46%);
  .container {
    padding: ${rem(60)} ${rem(40)} ${rem(40)};
    @media (min-width: 1280px) {
      padding: ${rem(100)} ${rem(240)};
    }
    @media (max-width: 767px) {
      padding: ${rem(60)} ${rem(30)};
    }
    @media (min-width: 2560px) {
      width: ${rem(1140)};
    }
  }
  p {
    font-size: ${rem(14)};
    font-family: gilroyregular;
  }
`;

export const Wrapper = styled.div`
  margin-bottom: ${rem(20)};
  border-bottom: ${(props) =>
    props.last ? "" : "1px solid rgb(18, 18, 18, 0.2)"};
  padding-bottom: ${rem(25)};
  .content {
    position: relative;
    height: 0;
    overflow: hidden;
    -webkit-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
    font-family: gilroyregular;
    font-size: ${rem(14)};
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.43;
    letter-spacing: 0px;
    text-align: left;
    color: #484848;
  }
  .toggleWrapper {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }
`;

export const Button = styled.button`
  position: relative;
  &.toggle {
    width: 100%;
    background-color: transparent;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    font-family: gilroymedium;
    font-size: ${rem(14)};
    color: #484848;
    font-weight: 600;
    border: none;
    text-align: left;
    outline: none;
    cursor: pointer;
    padding: 10px 0;
  }
  @media (min-width: 360px) {
    font-size: ${rem(16, "small")};
  }
`;

export const H4 = styled.h4`
  flex-grow: 0;
  margin: 0 0 ${rem(24)};
  font-family: gilroyextrabold;
  text-transform: none;
  font-size: ${rem(32)};
  font-weight: 800;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: -1px;
  text-align: center;
  color: #484848;
  @media (max-width: 767px) {
    font-size: ${rem(32)};
  }
`;
