import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  SocialIcon,
  Column,
  SubHeading,
  FooterLink,
  Copyright,
  SocialRow,
} from "./style";
import Image from "next/image";
import Link from "next/link";
import Icons from "../Icons";
import { gumletLoaderBasic } from "../../utils/gumletLoader";
const Footer = () => {
  const [device, setDevice] = useState("");
  useEffect(() => {
    if (window.innerWidth >= 768) {
      setDevice("desktop");
    } else {
      setDevice("mobile");
    }
  }, []);
  return (
    <Container>
      <Row>
        <Column>
          <Link href={"https://fundfolio.com"}>
            <a rel="noopener noreferrer" target={"_blank"}>
              <Icons
                name="fundfolioLogo"
                deviceType={device}
                alt="fundfolioLogo"
              />
            </a>
          </Link>
          <p className="desc">
            kinfra hi-tech park
            <br />
            kalamassery, kochi
            <br />
            kerala 683503
          </p>
          <SocialRow>
            <SocialIcon
              href={"https://www.linkedin.com/showcase/marketfeed-app/"}
              target={"_blank"}
              rel="noopener noreferrer"
            >
              <Image
                loader={gumletLoaderBasic}
                src={"/Linkedin_Logo.svg"}
                width={26}
                height={26}
                alt={"linkedIn"}
              />
            </SocialIcon>
            <SocialIcon
              href={"https://twitter.com/MarketfeedNews"}
              target={"_blank"}
              rel="noopener noreferrer"
            >
              <Image
                loader={gumletLoaderBasic}
                src={"/twitter.svg"}
                width={26}
                height={26}
                alt={"twitter"}
              />
            </SocialIcon>
            <SocialIcon
              href={"https://www.instagram.com/marketfeed.news/"}
              target={"_blank"}
              rel="noopener noreferrer"
            >
              <Image
                loader={gumletLoaderBasic}
                src={"/instagram.svg"}
                width={26}
                height={26}
                alt={"instagram"}
              />
            </SocialIcon>
            <SocialIcon
              href={"https://www.youtube.com/channel/UCmapkf4A5883lT90j86WXFg"}
              target={"_blank"}
              rel="noopener noreferrer"
            >
              <Image
                loader={gumletLoaderBasic}
                src={"/you-tube.svg"}
                width={26}
                height={26}
                alt={"youtube"}
              />
            </SocialIcon>
          </SocialRow>
        </Column>
        <Column>
          <SubHeading>products</SubHeading>
          <Link href="https://marketfeed.news/">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>marketfeed.news</FooterLink>
            </a>
          </Link>
          <Link href="https://marketfeed.app">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>marketfeed.app</FooterLink>
            </a>
          </Link>
          <Link href="https://fundfolio.com/grow/">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>ITR for traders</FooterLink>
            </a>
          </Link>
        </Column>
        <Column>
          <SubHeading>company</SubHeading>
          <Link href="https://payments.marketfeed.com/disclaimer/">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>disclaimer</FooterLink>
            </a>
          </Link>
          <Link href="https://payments.marketfeed.com/privacy-policy/">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>privacy policy</FooterLink>
            </a>
          </Link>
          <Link href="https://payments.marketfeed.com/terms-of-use/">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>terms & conditions</FooterLink>
            </a>
          </Link>
          <Link href="https://payments.marketfeed.com/refund-policy/">
            <a rel="noopener noreferrer" target={"_blank"}>
              <FooterLink>refund policy</FooterLink>
            </a>
          </Link>
        </Column>
      </Row>
      <Row>
        <Copyright>
          <span>
            made in kerala
            {device === "desktop" ? (
              <Icons name="coconut" width={36} height={36} />
            ) : (
              <Icons name="coconut" />
            )}
          </span>
          <Link href="https://fundfolio.com/">
            <a rel="noopener noreferrer">
              <span
                style={{ opacity: "0.3", color: "white", marginTop: "55px" }}
              >
                © fundfolio
              </span>
            </a>
          </Link>
        </Copyright>
      </Row>
    </Container>
  );
};

export default Footer;
