import React, { useEffect, useState } from "react";
import { Container, Row, NavBar, Button, Section, WhiteBar } from "./style";
import Link from "next/link";
import Icons from "../Icons";
import { tracker } from "../../utils/tracker";

const Header = (props) => {
  const [device, setDevice] = useState("");
  const [fill, setFill] = useState("none");
  const [widthSize, setWidthSize] = useState(0);
  const [heightSize, setheightSize] = useState(0);
  const [color, setColor] = useState("none");

  useEffect(() => {
    if (device === "desktop") {
      setWidthSize("190");
      setheightSize("49");
    } else {
      setWidthSize("150");
      setheightSize("30");
    }
  }, [device]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  useEffect(() => {
    if (window.innerWidth > 767) {
      setDevice("desktop");
    } else {
      setDevice("mobile");
    }
  }, []);

  const onClickHandler = () => {
    const doc = document.getElementById("programsOffered");
    window.scroll({
      top: doc.offsetTop - 20,
      behavior: "smooth",
    });
    tracker("join_next_batch", { cta_from: "header" });
  };

  const handleScroll = () => {
    if (window.scrollY > 0) {
      document.getElementById("header").classList.add("scroll");
      setColor("#ffffffef");
      if (
        (window.scrollY > 485 && window.scrollY < 1250) ||
        window.scrollY > 2925
      ) {
        document.getElementById("headerCTA").classList.add("active");
        setFill("#ffff");
      } else {
        document.getElementById("headerCTA").classList.remove("active");
        setFill("none");
      }
    } else {
      document.getElementById("header").classList.remove("scroll");
      setColor("none");
    }
  };
  return (
    <Section>
      <Container transparent={props.transparent} id="header">
        {/* <WhiteBar>
          <svg>
            <filter id="blurMe">
              <feGaussianBlur in="SourceGraphic" stdDeviation="10" />
            </filter>
            <rect
              width="1280px"
              height="100px"
              style={{ fill: color }}
              rx="24"
              filter="url(#blurMe)"
            />
          </svg>
        </WhiteBar> */}
        <Link href="https://get.marketfeed.app/8GbT/cb8a5790">
          <a rel="noopener noreferrer" target={"_blank"} className="logo">
            <Icons
              name="marketfeedUniversityLogo"
              width={widthSize}
              height={heightSize}
              alt={"marketfeed Logo"}
            />
          </a>
        </Link>
        <Row>
          <NavBar width={device === "mobile" ? "60" : "90"}>
            <div className="downloadStore">
              <Link href="https://get.marketfeed.app/8GbT/cb8a5790">
                <a rel="noopener noreferrer" target={"_blank"} alt={"appstore"}>
                  <Icons
                    name="appStore"
                    width={device === "desktop" ? "36" : "24"}
                    fill={fill}
                    alt={"appstore"}
                  />
                </a>
              </Link>
              <Link href="https://get.marketfeed.app/8GbT/cb8a5790">
                <a
                  rel="noopener noreferrer"
                  target={"_blank"}
                  alt={"playstore"}
                >
                  <Icons
                    name="playStore"
                    width={device === "desktop" ? "36" : "24"}
                    fill={fill}
                    alt={"playstore"}
                  />
                </a>
              </Link>
            </div>
            <Link href={"https://marketfeed.app/open-demat-account"}>
              <a
                rel="noopener noreferrer"
                target={"_blank"}
                className="sub-broking"
              >
                New to trading?
              </a>
            </Link>
            <Button id="headerCTA" onClick={() => onClickHandler()}>
              <span>
                Join <span className="desktop">Next Batch</span>{" "}
              </span>
            </Button>
          </NavBar>
        </Row>
      </Container>
    </Section>
  );
};

export default Header;
