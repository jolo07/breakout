import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Section = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 9;
`;

export const WhiteBar = styled.div`
  width: ${rem(1240)};
  height: ${rem(80)};
  position: fixed;
  z-index: -1;
  filter: blur(1px);
  border-radius: 24px;
  left: 0;
  right: 0;
  margin: ${rem(20)} auto;
  overflow: hidden;
  
  svg, rect {
    width: ${rem(1280)};
    height: ${rem(100)};
    -webkit-backdrop-filter: blur(80px);
    backdrop-filter: blur(80px);
    @media (max-width: 767px) {
      margin-top: ${rem(50)};
      width: 95vw;
      height: ${rem(51.2)};
      border-radius: 10px;
    }
  }

  rect {
    position: absolute;
  }
`;

export const Container = styled.div`
  width: ${rem(1240)};
  height: ${rem(100)};
  position: fixed;
  top: 0;
  z-index: 2;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${rem(34)} ${rem(30)};
  margin-top: ${rem(20)};
  border-radius: 24px;
  border: 0px solid rgba(72, 72, 72, 0.04);

  &.scroll {
    background: rgba(255, 255, 255, 0.98);
    border: 2px solid rgba(72, 72, 72, 0.03);
    border-radius: 28px;
    height: ${rem(80)};
    transition: all 0.5s ease;
    padding: ${rem(34)} ${rem(12)} ${rem(34)} ${rem(30)};
    @media (max-width: 767px) {
      padding: 0 ${rem(10)} 0 0;
      height: ${rem(51.2)};
    }
  }

  .block {
    display: none;
    @media (min-width: 768px) {
      display: flex;
    }
  }

  .logo {
    margin-top: 28px;
    @media (max-width: 767px) {
      margin-top: 15px;
    }
  }

  @media (max-width: 767px) {
    padding: 0 10px 0 0;
    width: 95vw;
    margin-top: ${rem(10)};
    height: ${rem(51.2)};
    border-radius: 10px;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    padding: 0 20px;
    width: 95vw;
    margin-top: ${rem(10)};
  }
`;

export const NavBar = styled.div`
  display: flex;
  .downloadStore {
    width: ${(props) => (props.width ? rem(props.width) : rem(90))};
    display: flex;
    justify-content: space-between;
    align-items: center;
    svg:hover {
      fill: #ffff;
      path {
        fill: #637bff;
      }
    }
  }
  a {
    text-decoration: none;
  }

  .sub-broking {
    font-family: gilroybold;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 22px;
    letter-spacing: -0.5px;
    color: #484848;
    display: none;
    @media (min-width: 768px) {
      display: flex;
    }
  }

  @media (min-width: 768px) {
    .downloadStore {
      width: fit-content;
      display: flex;
      justify-content: space-between;
    }
    a {
      margin: 0 ${rem(20)} 0 0;
      text-decoration: none;
      font-family: gilroybold;
      display: flex;
      align-items: center;
      font-style: normal;
      font-weight: normal;
      line-height: 22px;
      letter-spacing: -0.5px;
      color: #ffffff;
      font-size: 16px;
    }
    a.active {
      color: #f55240;
    }
  }
  @media (min-width: 1920px) {
    a {
      font-size: ${rem(18)};
      margin: 0 ${rem(20, "large")} 0 0;
    }
  }
`;
export const Row = styled.div`
  display: flex;
`;

export const Button = styled.div`
  display: none;
  display: flex;
  align-items: center;
  justify-content: center;
  border: solid 1px #14cc60;
  z-index: 1;
  margin: 0 0 0 ${rem(15)};
  cursor: pointer;
  font-family: gilroybold;
  width: ${rem(197)};
  height: ${rem(54)};
  border-radius: 16px;
  font-size: ${rem(16)};
  color: #484848;
  span {
    display: flex;
    & > span {
      margin-left: 4px;
    }
  }
  @media (min-width: 768px) and (max-width: 1024px) {
    font-size: 10px;
  }
  & .desktop {
    display: none;
    @media (min-width: 768px) {
      display: block;
    }
  }
  @media (max-width: 768px) {
    width: ${rem(90)};
    height: ${rem(35)};
    border-radius: 10px;
  }

  &.active {
    border: solid 1px rgb(255, 255, 255, 0.2);
    background-color: #14cc60;
    color: #ffff;
  }
  &:hover {
    border: solid 1px rgb(255, 255, 255, 0.2);
    background-color: #14cc60;
    color: #ffff;
  }
  @media (min-width: 1440px) {
    font-size: ${rem(18)};
  }
`;
