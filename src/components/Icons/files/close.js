import React, { useState } from "react";
import { imgRemConverter } from "../../../utils/remConverter";

export const close = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    fill="none"
    viewBox="0 0 24 24"
  >
    <rect
      width="22.5"
      height="22.5"
      x=".75"
      y=".75"
      stroke="#484848"
      strokeWidth="1.5"
      rx="11.25"
    />
    <path
      fill="#484848"
      fillRule="evenodd"
      d="M12.854 12.003l3.171 3.172c.235.235.235.615 0 .85-.235.234-.615.234-.85-.001l-3.171-3.173-3.179 3.172c-.235.235-.615.235-.85 0-.234-.235-.234-.615.001-.85l3.179-3.171-3.176-3.177c-.235-.235-.234-.615 0-.85.235-.234.615-.234.85.001l3.176 3.177 3.17-3.163c.234-.234.614-.234.849.001.234.235.234.615 0 .85l-3.17 3.162z"
      clipRule="evenodd"
    />
  </svg>
);
