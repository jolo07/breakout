import React from "react";
export const enrollmentIcon = (props) => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4.68421 8.51309H6.22921V12.0318C6.22921 12.8529 6.68421 13.019 7.23921 12.4032L11.0242 8.20032C11.4892 7.68717 11.2942 7.26199 10.5892 7.26199H9.04422V3.74327C9.04422 2.92223 8.58922 2.75607 8.03422 3.37185L4.24921 7.57477C3.78921 8.0928 3.98421 8.51309 4.68421 8.51309Z"
        stroke="#E92862"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
