import { coconut } from "./coconut";
import { whatsapp } from "./whatsapp";
import { minus } from "./minus";
import { plus } from "./plus";
import { talkToUsOverlay } from "./talkToUsOverlay";
import { discord } from "./discord";
import { pattern } from './pattern';
import { close } from "./close";
import { appStore } from "./appStore";
import { playStore } from "./playStore";
import { marketfeedUniversityLogo } from "./marketfeedUniversityLogo";
import { tooltip } from "./tooltip";
import { fundfolioLogo } from "./fundfolioLogo";
import { cardRibbon } from "./cardRibbon";
import { cardRibbonHighValue } from "./cardRibbonHighValue";
import { bulletPoint } from "./bulletPoint";
import { forwardIcon } from "./forwardIcon";
import { seatIcon } from "./seatIcon";
import { enrollmentIcon } from "./enrollmentIcon";
import { experienceIcon } from "./experienceIcon";
import { clientIcon } from "./clientIcon";
import { aumIcon } from "./aumIcon";

export default {
  close,
  discord,
  coconut,
  minus,
  plus,
  talkToUsOverlay,
  whatsapp,
  discord,
  pattern,
  marketfeedUniversityLogo,
  tooltip,
  cardRibbon,
  cardRibbonHighValue,
  appStore,
  playStore,
  fundfolioLogo,
  bulletPoint,
  forwardIcon,
  enrollmentIcon,
  seatIcon,
  experienceIcon,
  clientIcon,
  aumIcon
};
