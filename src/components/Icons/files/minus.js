import React, { useState } from "react";
import { imgRemConverter } from "../../../utils/remConverter";
export const minus = (props) => {
  const [widthSize, setWidthSize] = useState(0);
  const [heightSize, setHeightSize] = useState(0);
  React.useEffect(() => {
    setWidthSize(imgRemConverter("13"));
    setHeightSize(imgRemConverter("13"));
  });
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="4"
      fill="none"
      viewBox="0 0 13 4"
    >
      <path
        fill="#637BFF"
        d="M11.75.438H1.25c-.492 0-.875.41-.875.875v.875c0 .492.383.874.875.874h10.5c.465 0 .875-.382.875-.874v-.876c0-.464-.41-.875-.875-.875z"
      />
    </svg>
  );
};
