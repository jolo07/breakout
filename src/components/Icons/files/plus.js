import React, { useState } from "react";
import { imgRemConverter } from "../../../utils/remConverter";
export const plus = () => {
  const [widthSize, setWidthSize] = useState(0);
  const [heightSize, setHeightSize] = useState(0);
  React.useEffect(() => {
    setWidthSize(imgRemConverter("13"));
    setHeightSize(imgRemConverter("13"));
  });

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="13"
      fill="none"
      viewBox="0 0 13 13"
    >
      <path
        fill="#637BFF"
        d="M11.75 5.438H7.812V1.5c0-.465-.41-.875-.875-.875h-.875c-.492 0-.875.41-.875.875v3.938H1.25c-.492 0-.875.41-.875.875v.875c0 .492.383.875.875.875h3.938V12c0 .492.382.875.875.875h.875c.464 0 .875-.383.875-.875V8.062h3.937c.465 0 .875-.382.875-.874v-.875c0-.465-.41-.875-.875-.875z"
      />
    </svg>
  );
};
