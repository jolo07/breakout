import React from "react";
export const seatIcon = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
    >
      <path
        d="M6.964 3.26 3.768 5.19c-1.024.618-1.024 2.002 0 2.62l3.196 1.929c.573.348 1.518.348 2.092 0l3.18-1.929c1.019-.618 1.019-1.997 0-2.616l-3.18-1.928c-.574-.354-1.519-.354-2.092-.005z"
        stroke="#E92862"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.005 9 5 11.104c0 .57.46 1.18 1.025 1.36l1.5.475c.259.081.687.081.95 0l1.5-.475c.564-.18 1.025-.79 1.025-1.36V9.022M13 10V7"
        stroke="#E92862"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
