import React, { useState } from "react";
import { imgRemConverter } from "../../../utils/remConverter";

export const talkToUsOverlay = (props) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1226"
      height="1804"
      fill="none"
      viewBox="0 0 1226 1804"
    >
      <path
        fill="url(#paint0_linear)"
        d="M1764.15 703.276c57.12 217.658 58.01 408.014 2.66 571.054-55.35 163.05-166.95 357.81-334.81 584.29-167.85 226.48-354.91 240.31-561.166 41.47-206.258-198.83-369.086-430.73-488.482-695.69-119.397-264.964-55.35-473.178 192.143-624.644 247.492-151.465 493.355-223.16 737.605-215.083 244.24 8.078 394.92 120.945 452.05 338.603z"
        opacity=".03"
      />
      <path
        fill="url(#paint1_linear)"
        d="M1491.68 1092.65c-40.49 148.97-105.08 262.47-193.77 340.49-88.69 78.02-221.68 155.65-398.977 232.91-177.293 77.25-293.255 21.48-347.886-167.32-54.63-188.8-72.123-382.4-52.479-580.8 19.644-198.404 128.959-300.307 327.946-305.71 198.986-5.402 369.716 36.076 512.196 124.434 142.47 88.358 193.46 207.023 152.97 355.996z"
        opacity=".03"
      />
      <path
        fill="url(#paint2_linear)"
        d="M1274.82 853.283c19.48 97.499 14.89 181.477-13.8 251.947s-83.02 153.45-162.99 248.95c-79.97 95.5-162.835 96.7-248.591 3.6-85.757-93.1-151.491-199.64-197.203-319.64-45.713-119.99-12.004-210.151 101.124-270.48 113.129-60.329 223.449-85.516 330.96-75.559 107.51 9.956 171.01 63.684 190.5 161.182z"
        opacity=".05"
      />
      <defs>
        <linearGradient
          id="paint0_linear"
          x1="-30.468"
          x2="2037.6"
          y1="1056.82"
          y2="1191.4"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#3682EB" />
          <stop offset="1" stopColor="#5145C1" />
        </linearGradient>
        <linearGradient
          id="paint1_linear"
          x1="303.57"
          x2="1487.31"
          y1="688.951"
          y2="1476.46"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#3682EB" />
          <stop offset="1" stopColor="#5145C1" />
        </linearGradient>
        <linearGradient
          id="paint2_linear"
          x1="474.031"
          x2="1382.62"
          y1="962.245"
          y2="1075.82"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#3682EB" />
          <stop offset="1" stopColor="#5145C1" />
        </linearGradient>
      </defs>
    </svg>
);
