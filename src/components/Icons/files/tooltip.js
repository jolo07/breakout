import React from "react";

export const tooltip = (props) => {

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.width}
      fill="none"
      viewBox="0 0 24 24"
    >
      <circle cx="12" cy="12" r="7.5" stroke="#C6C6D7" />
      <path
        fill="#5451D7"
        d="M11.7 10.267c-.228 0-.427-.081-.598-.245-.163-.17-.245-.37-.245-.597 0-.228.082-.427.245-.598.171-.17.37-.256.598-.256.234 0 .434.086.597.256.17.171.256.37.256.598 0 .227-.085.426-.256.597-.163.164-.363.245-.597.245zm-.683 5.974v-5.334h1.376v5.334h-1.376z"
      />
    </svg>
  );
};
