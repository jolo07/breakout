import React, { useEffect, useState } from "react";
import Icons from "../Icons";
import {
  Container,
  Backdrop,
  ModalCloseButton,
  Header,
  RadioButton,
  Wrapper,
  Button,
} from "./style";

const LanguageModel = ({ visible, close, programSelected }) => {
  const [languageSelected, setLanguageSelected] = useState("malayalam");
  const productWooCoomerceLink = {
    options: {
      english:
        "https://breakout.fundfolio.in/product/option-selling-by-sharique-samsudheen-english/",
      malayalam:
        "https://breakout.fundfolio.in/product/option-selling-by-sharique-samsudheen-malayalam/",
    },
    basicsPremium: {
      english: "",
      malayalam: "https://breakout.fundfolio.in/product/stock-market-basics/",
    },
    swing: {
      english: "",
      malayalam: "https://breakout.fundfolio.in/product/swing-trading/",
    },
  };

  useEffect(() => {
    document.addEventListener("keydown", function (event) {
      if (event.key === "Escape") {
        handleClose();
      }
    });
  });

  const navigateToPage = () => {
    window.open(
      productWooCoomerceLink[programSelected][languageSelected],
      "_blank"
    );
  };

  const handleActiveLanguage = (languageSelected) => {
    let active;
    if (programSelected === "swing" || programSelected === "basicsPremium") {
      active = languageSelected === "english" ? false : true;
    } else {
      active = true;
    }
    return active;
  };

  const handleClose = () => {
    setLanguageSelected("malayalam");
    close();
  };

  return (
    <>
      <Backdrop visible={visible} onClick={handleClose} />
      <Container visible={visible}>
        <div className="closeWrapper">
          <ModalCloseButton visible={visible} onClick={handleClose}>
            <Icons name="close" width="40" height="40" />
            <div className="closeText">ESC</div>
          </ModalCloseButton>
        </div>
        <div className="title">Choose your language</div>
        <Header>Select the comfortable language you want to learn in</Header>
        <Wrapper>
          <RadioButton
            className={languageSelected === "malayalam" ? "active" : ""}
            onClick={() =>
              handleActiveLanguage("malayalam")
                ? setLanguageSelected("malayalam")
                : null
            }
            active={handleActiveLanguage("malayalam")}
          >
            Malayalam
          </RadioButton>
          <RadioButton
            className={
              languageSelected === "english" ? "active" : "englishButton"
            }
            onClick={() =>
              handleActiveLanguage("english")
                ? setLanguageSelected("english")
                : null
            }
            program={programSelected}
            active={handleActiveLanguage("english")}
          >
            English
            {programSelected !== "options" ? (
              <span style={{ fontSize: "14px", fontFamily: "gilroybold" }}>
                &nbsp;(Coming Soon)
              </span>
            ) : null}
          </RadioButton>
        </Wrapper>
        <Button disabled={languageSelected} onClick={() => navigateToPage()}>
          Confirm
        </Button>
      </Container>
    </>
  );
};

export default LanguageModel;
