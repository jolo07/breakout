import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Container = styled.div`
  width: ${rem(600)};
  display: ${(props) => (props.visible ? "block" : "none")};
  transform: ${(props) => (props.visible ? null : "translateY(100%)")};
  position: fixed;
  top: 50%; /* position the top  edge of the element at the middle of the parent */
  left: 50%; /* position the left edge of the element at the middle of the parent */
  transform: translate(-50%, -50%);
  background: #ffffff;
  border-radius: 10px;
  color: #0b1721;
  z-index: 1500;
  padding: ${rem(80)};
  transition: all 0.3s ease-out;
  .closeWrapper {
    position: relative;
  }
  .img {
    object-fit: contain;
  }
  .imageContainer {
    border-radius: ${rem(12)};
    text-align: center;
  }
  .wrapper {
    text-align: center;
  }
  @media (max-width: 767px) {
    border-radius: 0;
    padding: ${rem(16)};
    width: calc(100vw - 40px);
    top: 0;
    bottom: 0;
    margin: auto;
    left: 20px;
    transform: none;
    height: ${rem(360)};
    border-radius: 20px;
  }
  .title {
    font-family: gilroyextrabold;
    font-size: 40px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: -1px;
    text-align: left;
    color: #484848;
    @media (max-width: 767px) {
      font-size: 28px;
      padding: ${rem(20)} ${rem(40)} 0;
      text-align: center;
      line-height: ${rem(36.4)};
    }
  }
`;
export const Header = styled.h4`
  color: #484848;
  font-size: ${rem(16)};
  font-family: "gilroymedium";
  opacity: 0.6;
  padding-top: 12px;
  padding-bottom: 24px;
  text-align: left;
  @media (max-width: 767px) {
    font-size: 14px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: ${rem(21)};
    letter-spacing: normal;
    text-align: center;
    color: #7f7f7f;
    margin-top: ${rem(8)};
    padding: ${rem(0)} ${rem(30)} ${rem(24)};
  }
`;
export const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  min-height: ${(props) => (props.visible ? "100vh" : "0%")};
  background-color: rgba(0, 0, 0, 0.45);
  opacity: ${(props) => (props.visible ? 1 : 0)};
  filter: alpha(opacity=45);
  transition: opacity 0.3s linear, height 0s ease 0.3s;
  pointer-events: ${(props) => (props.visible ? "auto" : "none")};
  cursor: pointer;
  z-index: 999;
`;
export const ModalCloseButton = styled.div`
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  top: -56px;
  right: -56px;
  width: 24px;

  .closeText {
    font-size: 16px;
    font-family: gilroymedium;
    color: #484848;
    letter-spacing: -0.5px;
    opacity: 0.3;
    margin-top: 4px;
    text-align: center;
    @media (max-width: 767px) {
      display: none;
    }
  }
  @media (max-width: 767px) {
    top: 0px;
    right: 0px;
    height: 16px;
    width: 16px;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 440px;
  @media (max-width: 767px) {
    width: 100%;
    flex-direction: column;
  }
`;

export const RadioButton = styled.div`
  flex: 1;
  height: 56px;
  background:${(props) => {
    return props.active ? "#fff" : "#cccccc";
  }};
  box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.04);
  border: solid 1px #e4e5eb;
  font-family: ${(props) =>
    props.program !== "options" ? "gilroymedium" : "gilroybold"};
  border-radius: 10px;
  text-align: center;
  line-height: 56px;
  font-size: 16px;
  color: #484848;
  cursor:${(props) => {
    return props.active ? "pointer" : "no-drop";
  }};
  &:nth-child(1){
      margin-right: 10px;
  }
  @media (max-width: 767px) {
        height: 40px;
        line-height: 40px;
        border-radius: 10px;
        font-size: 14px;
      &:nth-child(1){
          margin-right: 0px;
          margin-bottom: 12px;
      }

  }

  &.active{
    border: solid 1px #14cc60;
    color: #484848; 
    font-family: gilroybold;
  }
  &.englishButton {
    line-height: ${(props) => (props.program !== "options" ? "" : "56px")};
  }
}

`;

export const Button = styled.div`
  width: 440px;
  text-align: center;
  margin-top: 36px;
  background-color: #14cc60;
  line-height: 56px;
  border-radius: 16px;
  font-size: 18px;
  letter-spacing: -0.5px;
  text-align: center;
  color: #ffffff;
  font-family: gilroybold;
  cursor: pointer;
  padding: 0 0 2px 0;
  @media (max-width: 767px) {
    width: 100%;
    margin-top: 20px;
    height: 40px;
    font-size: 14px;
    line-height: 40px;
    border-radius: 10px;
  }
`;
