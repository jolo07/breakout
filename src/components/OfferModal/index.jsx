import React, { useEffect, useState } from "react";
import Image from "next/image";
import Icons from "../Icons";
import OfferPage1 from "../OfferPage1";
import OfferPage2 from "../OfferPage2";
import OfferPage3 from "../OfferPage3";
import OfferPage4 from "../OfferPage4";
import OfferPage5 from "../OfferPage5";
import {
  Container,
  Backdrop,
  ChangeContainer,
  ModalCloseButton,
} from "./style";

const OfferModal = ({ visible, close }) => {
  const [activePage, setActivePage] = useState(0);
  const [couponStatus, setCouponStatus] = useState();
  const [userName, setUserName] = useState();
  const [userStatus, setUserStatus] = useState();
  useEffect(() => {
    document.addEventListener("keydown", function (event) {
      if (event.key === "Escape") {
        close();
      }
    });
  });

  useEffect(() => {
    let element = document.documentElement;
    if (visible) {
      element.style.overflow = "hidden";
    }
    return () => {
      element.style.overflow = "scroll";
    };
  }, [visible]);

  useEffect(() => {
    if (couponStatus === "AVAILABLE") {
      setActivePage(1);
    }
    if (couponStatus === "REDEEMED") {
      setActivePage(3);
    }
    if (couponStatus === "PENDING") {
      setActivePage(4);
    }
    if (couponStatus === "NEW") {
      setActivePage(2);
    }
  }, [userStatus, couponStatus]);

  const handleClose = () => {
    if (activePage === 2) setActivePage(0);
    close();
  };

  return (
    <>
      <Backdrop visible={visible} onClick={() => handleClose()} />
      <Container visible={visible}>
        <div className="closeWrapper">
          <ModalCloseButton visible={visible} onClick={() => handleClose()}>
            <Icons name="close" width="40" height="40" />
          </ModalCloseButton>
          <span className="closeButtonText">ESC</span>
        </div>
        <div>
          {activePage === 0 ? (
            <OfferPage1
              setUserStatus={setUserStatus}
              setCouponStatus={setCouponStatus}
              setUserName={setUserName}
              visible={visible}
            />
          ) : null}
          {activePage === 1 ? (
            <OfferPage2 userName={userName} couponStatus={couponStatus} />
          ) : null}
          {activePage === 2 ? <OfferPage3 /> : null}
          {activePage === 3 ? <OfferPage4 /> : null}
          {activePage === 4 ? <OfferPage5 /> : null}
        </div>
      </Container>
    </>
  );
};

export default OfferModal;
