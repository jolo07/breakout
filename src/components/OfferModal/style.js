import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  min-height: ${(props) => (props.visible ? "100vh" : "0%")};
  background-color: rgba(0, 0, 0, 0.45);
  opacity: ${(props) => (props.visible ? 1 : 0)};
  filter: alpha(opacity=45);
  transition: opacity 0.3s linear, height 0s ease 0.3s;
  pointer-events: ${(props) => (props.visible ? "auto" : "none")};
  cursor: pointer;
  z-index: 999;
`;

export const Container = styled.div`
  width: ${rem(600)};
  display: ${(props) => (props.visible ? "block" : "none")};
  transform: ${(props) => (props.visible ? null : "translateY(100%)")};
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: #ffffff;
  border-radius: 10px;
  color: #0b1721;
  z-index: 1500;
  overflow: scroll;
  padding: ${rem(40)};
  transition: all 0.3s ease-out;
  scrollbar-width: none;
  .closeButtonText {
    color: #484848;
    font-family: gilroymedium;
    font-size: 16px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -0.5px;
    opacity:0.3;
    position: absolute;
    right: -24px;
    text-align: center;
    top: 10px;
    @media (max-width: 767px) {
      display: none;
    }
  }
  .closeWrapper {
    position: relative;
  }
  .img {
    object-fit: contain;
  }
  .imageContainer {
    border-radius: ${rem(12)};
    text-align: center;
  }
  .wrapper {
    text-align: center;
  }
  @media (max-width: 767px) {
    border-radius: 0;
    padding: ${rem(16)};
    width: 100vw;
    height: 100vh;
    top: 0;
    left: 0;
    transform: none;
  }
`;

export const ChangeContainer = styled.span`
  background-color: ${(props) => (props.change > 0 ? "#1bcc7b" : "#e84e4f")};
  border-radius: 5px;
  display: ${(props) => (props.visible ? "block" : "none")};
  padding: 3px;
`;

export const ModalCloseButton = styled.div`
  cursor: pointer;
  display: flex;
  position: absolute;
  top: -22px;
  right: -22px;
  height: 24px;
  width: 24px;
  @media (max-width: 767px) {
    top: 0px;
    right: 0px;
    height: 16px;
    width: 16px;
  }
`;
