import PhoneInput, {
  isValidPhoneNumber,
  isPossiblePhoneNumber,
} from "react-phone-number-input";
import firebase from "firebase/app";
import "firebase/auth";
import axios from "axios";
import React, { useState, useEffect, useRef } from "react";

import fire from "../../utils/fire";
import {
  addUserRecord,
  doesUserDetailsCollectionExist,
} from "../../services/db";
import Spinner from "../Spinner";
import createProfileImage from "../../utils/createProfileImage";
import {
  Container,
  Header,
  PhoneInputContainer,
  Input,
  InputContainer,
  CarouselContainer,
  CheckBoxContainer,
  ButtonContainer,
  BackButton,
  Button,
} from "./style";
import { rem } from "../../utils/remConverter";
const colors = {
  primary: "#ffc400",
  secondary: "#0b1721",
};

const OfferPage1 = (props) => {
  const [phoneNumber, setPhoneNumber] = useState("0");
  const [otp, setOtp] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [loginErrorMessage, setLoginErrorMessage] = useState("");
  const [signUpErrorMessage, setSignUpErrorMessage] = useState("");
  const [receivedOtp, setReceivedOtp] = useState(false);
  const [loading, setLoading] = useState(false);
  const [otpDisabled, setOtpDisabled] = useState(true);
  const [isDisabled, setIsDisabled] = useState(true);
  const [isChecked, setIsChecked] = useState(true);
  const [isSignUpButtonDisabled, setSignUpButtonDisabled] = useState(true);
  const [showSignUp, setShowSignUp] = useState(false);
  const [isResendActive, setIsResendActive] = useState(false);
  const [timerCount, setTimerCount] = useState(0);
  const [isTimerActive, toggleTimer] = useState(false);
  const [timerId, setTimerId] = useState();
  const phoneInput = useRef(null);

  // checks if user logged in on page load and
  // checks validity of phone number
  useEffect(() => {
    window.appVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
      }
    );
  }, []);

  useEffect(() => {
    checkPhoneNumber();
  }, []);

  useEffect(() => {
    if (!props.visible) resetModal();
  }, [props.visible]);

  useEffect(() => {
    checkPhoneNumber();
  }, [phoneNumber]);

  useEffect(() => {
    checkOtp();
  }, [otp]);

  useEffect(() => {
    handleSignUpButtonDisabled();
  }, [name, email]);

  useEffect(() => {
    if (timerCount > 0) {
      let tempTimerId = setTimeout(() => {
        setTimerCount(timerCount - 1);
      }, 1000);
      setTimerId(tempTimerId);
    }
    if (isTimerActive && timerCount === 0) {
      setIsResendActive(true);
      toggleTimer(false);
    }
  }, [isTimerActive, timerCount]);

  const checkOtp = () => {
    if (otp.length > 5 && !loading) {
      setOtpDisabled(false);
    } else {
      setOtpDisabled(true);
    }
  };

  const checkPhoneNumber = () => {
    try {
      if (
        isPossiblePhoneNumber(phoneNumber) &&
        isValidPhoneNumber(phoneNumber) &&
        isChecked
      ) {
        setIsDisabled(false);
      } else {
        setIsDisabled(true);
      }
    } catch (error) {}
  };

  const clearGarbage = () => {
    setTimerCount(0);
    setLoginErrorMessage("");
    setOtp("");
  };

  const formatTime = (time) => {
    let seconds = time % 60;
    let minutes = Math.floor(time / 60);
    minutes = minutes.toString().length === 1 ? "0" + minutes : minutes;
    seconds = seconds.toString().length === 1 ? "0" + seconds : seconds;
    return minutes + ":" + seconds;
  };

  const handlePhoneLogin = async (event) => {
    event.preventDefault();
    clearGarbage();
    if (!isResendActive) setLoading(true);
    const isPhoneNumberValid =
      phoneNumber && isPossiblePhoneNumber(phoneNumber);
    if (!isPhoneNumberValid) {
      setLoginErrorMessage("Enter a valid Number");
    } else if (isResendActive) {
      window.appVerifier.render().then((widgetId) => {
        window.recaptchaId = widgetId;
      });
      window.appVerifier = new firebase.auth.RecaptchaVerifier(
        "resend-recaptcha-container",
        {
          size: "invisible",
        }
      );
      //verifies captcha and logs in user
      const appVerifier = window.appVerifier;
      signInWithPhoneNumber(appVerifier);
    } else {
      setLoginErrorMessage("");
      window.appVerifier.reset();
      // Turn off phone auth app verification.
      const appVerifier = window.appVerifier;
      signInWithPhoneNumber(appVerifier);
    }
  };

  const handleResendOtp = (event) => {
    event.preventDefault();
    handlePhoneLogin(event);
    setIsResendActive(false);
  };

  const handleSignUp = async () => {
    let user = firebase.auth().currentUser;
    let profileImageUrl = createProfileImage(email);
    let userRecord = {
      email: email,
      name: name,
      mobile: phoneNumber,
      profileImage: profileImageUrl,
      role: "user",
    };

    const response = await addUserRecord(user.uid, userRecord);

    if (response) {
      setLoading(true);
      props.setUserStatus("newlyRegistered");
      props.setCouponStatus("NEW");
      setTimeout(() => {
        setLoading(false);
      }, 2500);
    } else {
      setSignUpErrorMessage("Unable to create your account. Please try again.");
    }
  };

  const handleSignUpButtonDisabled = () => {
    if (name == "") {
      setSignUpErrorMessage("Enter a name");
      setSignUpButtonDisabled(true);
    } else if (
      email === "" ||
      !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email
      )
    ) {
      // setEmailError("Enter a valid Email ID");
      setSignUpButtonDisabled(true);
    } else {
      setSignUpErrorMessage("");
      setSignUpButtonDisabled(false);
    }
  };

  const onVerifyCodeSubmit = async (event) => {
    event.preventDefault();

    setLoading(true);

    const verificationId = otp;
    window.confirmationResult
      .confirm(verificationId)
      .then(async function (result) {
        // User signed in successfully.
        let user = result.user;
        const userDetails = await doesUserDetailsCollectionExist(user.uid);
        user.getIdToken().then(async (idToken) => {
          if (userDetails.exists) {
            setLoading(true);
            setTimeout(() => {
              setLoading(false);
            }, 2500);
            var config = {
              method: "get",
              url: `${process.env.NEXT_PUBLIC_API_BASE_URL}/user/web-promo-preference?type=sub-broking-mf-university`,
              headers: {
                token: idToken,
                "Access-Control-Allow-Origin": "*",
              },
            };

            const data = await axios(config)
              .then(function (response) {
                return response.data;
              })
              .catch(function () {
                props.setCouponStatus("NEW");
              });

            props.setUserStatus(userDetails.role);
            props.setCouponStatus(data?.status || "NEW");
            localStorage.setItem("name", userDetails?.name);
            localStorage.setItem("couponCode", data?.couponCode);
          } else {
            setShowSignUp(true);
            clearGarbage();
          }
        });
        //if user role is advisor push page to dash
        // else push page to marketfeed login or download app page
      })
      .catch(function () {
        setLoading(false);
        setLoginErrorMessage("Error while checking the verification code");

        window.appVerifier.render().then(function (widgetId) {
          grecaptcha.reset(widgetId);
        });
      });
  };

  const resetModal = () => {
    clearGarbage();
    clearTimeout(timerId);
    setReceivedOtp(false);
    setPhoneNumber("");
    setShowSignUp(false);
    setIsResendActive(false);
    setTimerCount(0);
    setLoading(false);
    toggleTimer(false);
  };

  const signInWithPhoneNumber = (appVerifier) => {
    setTimeout(() => {
      setReceivedOtp(true);
      setLoading(false);
    }, 2000);

    fire
      .auth()
      .setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(() => {
        firebase
          .auth()
          .signInWithPhoneNumber(phoneNumber, appVerifier)
          .then(function (confirmationResult) {
            if (!isResendActive) {
              setTimerCount(60);
              toggleTimer(true);
            }

            window.confirmationResult = confirmationResult;
          })
          .catch(function (error) {
            if (error.code === "auth/too-many-requests") {
              setLoginErrorMessage(error.message);
            } else {
              setLoginErrorMessage(
                "* Sorry we have encountered an unexpected error, please refresh the page and try again"
              );
            }
          });
      })
      .catch(function () {
        setLoginErrorMessage(
          "* Sorry we have encountered an unexpected error, please refresh the page and try again"
        );
      });
  };

  return (
    <Container>
      {showSignUp ? (
        <>
          <div className="title">Claim your 50% Off</div>
          <Header>
            Verify your phone number to check whether you have a demat account
            with us or not
          </Header>
          <div>
            <PhoneInputContainer active={true} receivedOtp={true}>
              <PhoneInput
                className="phoneInput"
                defaultCountry="IN"
                disabled={true}
                maxLength="17"
                placeholder="Enter Mobile Number"
                value={phoneNumber}
                onChange={setPhoneNumber}
                international
                countryCallingCodeEditable={false}
              />
              <div className="resendOtp" onClick={() => resetModal()}>
                Change
              </div>
            </PhoneInputContainer>
            <InputContainer>
              <Input
                type="text"
                autoFocus
                required
                value={name}
                placeholder="Enter your name"
                className="userName"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </InputContainer>
            <InputContainer>
              <Input
                type="text"
                autoFocus
                required
                value={email}
                placeholder="Enter your Email id"
                className="email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </InputContainer>
            <p className="errorMsg">{signUpErrorMessage}</p>
            <ButtonContainer>
              <Button
                type={isSignUpButtonDisabled ? "secondary" : "primary"}
                onClick={handleSignUp}
                htmlType="submit"
                disabled={isSignUpButtonDisabled}
                $loading={loading}
              >
                Check Eligibility
              </Button>
            </ButtonContainer>
          </div>
        </>
      ) : (
        <>
          <div className="title">Claim your 50% Off</div>
          <Header>
            Verify your phone number to check whether you have a demat account
            with us or not
          </Header>
          <form onSumbit={(e) => handlePhoneLogin(e)}>
            <PhoneInputContainer
              active={isResendActive}
              receivedOtp={receivedOtp}
            >
              <PhoneInput
                className="phoneInput"
                countryCallingCodeEditable={false}
                defaultCountry="IN"
                international
                maxLength="17"
                placeholder="Enter Mobile Number"
                onChange={setPhoneNumber}
                value={phoneNumber}
              />
              <div
                className="resendOtp"
                onClick={isResendActive ? (e) => handleResendOtp(e) : null}
              >
                Resend OTP
              </div>
            </PhoneInputContainer>
            {!receivedOtp ? (
              <ButtonContainer>
                <Button
                  type={isDisabled ? "ghost" : "primary"}
                  id="sign-in-button"
                  $loading={loading}
                  onClick={(e) => handlePhoneLogin(e)}
                  disabled={isDisabled}
                >
                  <span>Send OTP</span>
                  <Spinner active={loading} />
                </Button>
              </ButtonContainer>
            ) : null}
          </form>
          {receivedOtp && (
            <div>
              <form onSumbit={(e) => onVerifyCodeSubmit(e)}>
                <InputContainer>
                  <Input
                    autoFocus
                    className="otpInput"
                    maxLength="6"
                    onChange={(e) => setOtp(e.target.value)}
                    placeholder="Enter OTP"
                    required
                    type="number"
                    value={otp}
                  />
                  {receivedOtp ? (
                    <p className="timer">{formatTime(timerCount)}</p>
                  ) : null}
                </InputContainer>
                {loginErrorMessage != "" ? (
                  <p className="errorMsg">{loginErrorMessage}</p>
                ) : null}
                <ButtonContainer>
                  <Button
                    type={otpDisabled ? "secondary" : "primary"}
                    onClick={(e) => onVerifyCodeSubmit(e)}
                    htmlType="submit"
                    disabled={otpDisabled}
                    $loading={loading}
                  >
                    <span>Verify Phone Number</span>
                    <Spinner active={loading} />
                  </Button>
                </ButtonContainer>
              </form>
            </div>
          )}
          <div id="recaptcha-container"></div>
          <div id="resend-recaptcha-container"></div>
        </>
      )}
    </Container>
  );
};

export default OfferPage1;
