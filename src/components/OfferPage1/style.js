import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Button = styled.button`
  border: none;
  border-radius: 16px;
  background-color: ${(props) =>
    props.test || props.disabled ? "#c8c8c8" : "#14cc60"};
  color: #fff;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  height: ${rem(54)};
  padding: 0 0 2px 0;
  font-family: gilroybold;
  font-size: ${rem(18)};
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.5px;
  margin-top: ${rem(36)};
  width: ${rem(440)};
  @media (max-width: 767px) {
    border-radius: 10px;
    height: ${rem(40)};
    font-size: ${rem(14)};
    margin-top: ${rem(24)};
    width: ${rem(288)};
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const CheckBoxContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${rem(12)};
  color: #121212;
  text-align: center;
  margin-top: ${rem(16)};
`;

export const Container = styled.div`
  align-items: flex-start;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: ${rem(40)};
  .errorMsg {
    color: red;
    font-size: 14px;
    margin-top: 15px;
    @media (max-width: 767px) {
      font-size: 12px;
    }
  }
  .title {
    font-family: gilroyextrabold;
    font-size: 40px;
    font-weight: 800;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: -1px;
    text-align: left;
    color: #484848;
    @media (max-width: 767px) {
      font-size: 28px;
      padding-top: ${rem(120)};
    }
  }
  @media (max-width: 767px) {
    align-items: center;
    padding: 0;
  }
`;

export const FormContainer = styled.div`
  background: rgba(255, 255, 255);
  margin: 0px 10px 20px;
  width: calc(100vw - 20px);
  border-radius: 5;
  maxwidth: 450;
  padding: 0px 10px;
`;

export const Header = styled.h4`
  color: #484848;
  font-size: ${rem(16)};
  font-family: "gilroymedium";
  opacity: 0.6;
  padding-top: 12px;
  padding-bottom: 24px;
  text-align: left;
  @media (max-width: 767px) {
    font-size: 14px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: #7f7f7f;
    margin-top: ${rem(8)};
    padding: ${rem(0)} ${rem(30)};
  }
`;

export const Input = styled.input`
  border: none;
  width: ${rem(360)};
  outline: none;
  font-family: gilroymedium;
  font-size: 16px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: ${(props) => (props.type === "text" ? "normal" : "1em")};
  text-align: ${(props) => (props.type === "text" ? "left" : "center")};
  color: #808080;
  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    letter-spacing: normal;
    text-align: left;
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    letter-spacing: normal;
    text-align: left;
  }
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  ::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  @media (max-width: 767px) {
    font-size: 14px;
    margin-right: 12px;
    height: ${rem(20)};
    width: ${rem(210)};
  }
`;

export const InputContainer = styled.div`
  align-items: center;
  border: 1px solid #e3e3e3;
  border-radius: 16px;
  color: #7f7f7f;
  display: flex;
  height: ${rem(56)};
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  margin-top: 16px;
  outline: none;
  padding: ${rem(20)};
  text-align: left;
  width: ${rem(440)};
  position: relative;
  input[type="number"]::-webkit-outer-spin-button,
  input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type="number"] {
    -moz-appearance: textfield;
  }
  .errorMsg {
    font-size: 12px;
    color: red;
    position: absolute;
    top: 60px;
    left: 0px;
    @media (max-width: 767px) {
      top: 40px;
      left: 0px;
    }
  }
  .timer {
    color: #637bff;
  }
  @media (max-width: 767px) {
    border-radius: 10px;
    font-size: 14px;
    height: ${rem(40)};
    padding: ${rem(12)};
    width: ${rem(288)};
  }
`;

export const PhoneInputContainer = styled.div`
  background-color: #fff;
  padding: 0 ${rem(20)};
  height: ${rem(56)};
  width: ${rem(440)};
  border-radius: 16px;
  border: 1px solid #e3e3e3;
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .PhoneInputInput.PhoneInputInput {
    align-items: center;
    color: #484848;
    background-color: #fff;
    border: none;
    border-left: 1px solid #e3e3e3;
    font-family: gilroymedium;
    font-size: ${rem(16)};
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    margin-bottom: ${rem(16)};
    margin-left: 4px;
    margin-top: ${rem(16)};
    padding-left: 4px;
    @media (max-width: 767px) {
      align-items: center;
      background-color: #fff;
      font-size: ${rem(14)};
      height: ${rem(11)};
      width: ${rem(135)};
    }
  }
  .PhoneInputInput.PhoneInputInput:focus {
    background-color: #fff;
    outline-width: 0px;
    outline-style: none;
  }
  .PhoneInputInput.PhoneInputInput:focus-visible {
    background-color: #fff;
    outline-width: 0px;
    outline-style: none;
  }
  .PhoneInputCountrySelectArrow.PhoneInputCountrySelectArrow {
    color: #121212;
    opacity: 1;
  }
  .resendOtp {
    color: ${(props) => (props.active ? "#14cc60" : "#c8c8c8")};
    cursor: ${(props) => (props.active ? "pointer" : "not-allowed")};
    display: ${(props) => (props.receivedOtp ? "block" : "none")};
    font-family: gilroybold;
    font-size: 18px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -0.5px;
    text-align: right;
    width: 100px;
    @media (max-width: 767px) {
      font-size: 14px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: -0.5px;
      text-align: center;
    }
  }
  @media (max-width: 767px) {
    padding: 0 ${rem(16)};
    height: ${rem(43)};
    width: ${rem(288)};
    border-radius: 10px;
    margin-top: ${rem(36)};
  }
`;
