import React, { useEffect, useState } from "react";
import Image from "next/image";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";

import CouponLayout from "../CouponLayout";
import { Container, Button } from "./style";
const OfferPage2 = ({ userName }) => {
  const [couponCode, setCouponCode] = useState("");
  useEffect(() => {
    getAndSetCouponCodeFromLocalStorage();
  }, []);

  const getAndSetCouponCodeFromLocalStorage = () => {
    const tempCouponCode = localStorage.getItem("couponCode")
      ? localStorage.getItem("couponCode")
      : "Invalid";
    setCouponCode(tempCouponCode);
  };

  const copy = () => {
    navigator.clipboard.writeText(couponCode);
    toast.success("🚀 Coupon Copied!", {
      position: "top-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    window.location.href =
      "https://breakout.fundfolio.in/product/breakout-basics/";
  };
  return (
    <>
      <Container>
        <div className="emojiDesktop">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/celebration_emoji.png"}
            width={100}
            height={100}
            alt={"stock market for beginners in india"}
          />
        </div>
        <div className="emojiMobile">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/celebration_emoji.png"}
            width={60}
            height={60}
            alt={"stock market for beginners in india"}
          />
        </div>
        <div className="title">
          Congrats {userName}!
          <br /> you are eligible for 50% Off
        </div>
        <div className="content">
          We have verified that you have a demat account with us, you are
          eligible for 50% for Stock Market Basics.
        </div>
        <div className="couponWrapperDesktop" onClick={() => copy()}>
          <CouponLayout
            discountPercentage="50"
            courseName="Stock Market Basics"
            currentPrice="5999"
            discountedPrice="2999"
            couponCode={couponCode}
          />
        </div>
        <div className="couponWrapperMobile" onClick={() => copy()}>
          <CouponLayout
            discountPercentage="50"
            courseName="Stock Market Basics"
            currentPrice="5999"
            discountedPrice="2999"
            couponCode={couponCode}
          />
        </div>
        <Button onClick={() => copy()}>Copy & Proceed</Button>
      </Container>
      <ToastContainer />
    </>
  );
};

export default OfferPage2;
