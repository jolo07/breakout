import React, { useEffect, useState } from "react";
import Image from "next/image";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import Icons from "../Icons";
import { Container, Button } from "./style";
const OfferPage3 = () => {
  return (
    <>
      <Container>
        <div className="emojiDesktop">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/rocket_emoji.png"}
            width={100}
            height={100}
            alt={"learn stock market trading"}
          />
        </div>
        <div className="emojiMobile">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/rocket_emoji.png"}
            width={60}
            height={60}
            alt={"learn stock market trading"}
          />
        </div>
        <div className="title">Open a demat account now and get 50% off</div>
        <div className="content">
          It seems you don't have a Demat Account with us, open an account now
          to get Stock Market Basics Premium at just Rs 2,999/year!
        </div>
        <Button
          onClick={() => {
            window.open(
              "https://marketfeed.app/open-demat-account?utm_source=marketfeedUniversity&utm_medium=couponGeneration",
              "_blank"
            );
          }}
        >
          Open Demat Account
        </Button>
        <Button
          type="ghost"
          onClick={() => {
            window.location.href =
              "https://breakout.fundfolio.in/product/breakout-basics/";
          }}
        >
          Continue without offer
        </Button>
      </Container>
    </>
  );
};

export default OfferPage3;
