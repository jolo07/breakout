import React, { useEffect, useState } from "react";
import Image from "next/image";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import Icons from "../Icons";
import { Container, Button } from "./style";
const OfferPage4 = () => {
  return (
    <>
      <Container>
        <div className="emojiDesktop">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/smile_round_emoji.png"}
            width={100}
            height={100}
            alt={"learn stock market trading "}
          />
        </div>
        <div className="emojiMobile">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/smile_round_emoji.png"}
            width={60}
            height={60}
            alt={"learn stock market trading "}
          />
        </div>
        <div className="title">Oh oh! You have already claimed this offer</div>
        <div className="content">
          Sorry, you have already redeemed this offer, Please proceed to pay to
          get Stock Market Basics.
        </div>
        <Button
          onClick={() => {
            window.location.href =
              "https://breakout.fundfolio.in/product/breakout-basics/";
          }}
        >
          Continue without offer
        </Button>
      </Container>
    </>
  );
};

export default OfferPage4;
