import React, { useEffect, useState } from "react";
import Image from "next/image";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import Icons from "../Icons";
import { Container, Button } from "./style";
const OfferPage5 = () => {
  return (
    <>
      <Container>
        <div className="emojiDesktop">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/sand_clock_emoji.png"}
            width={100}
            height={100}
            alt={"Best way to learn stock market"}
          />
        </div>
        <div className="emojiMobile">
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src={"/sand_clock_emoji.png"}
            width={60}
            height={60}
            alt={"Best way to learn stock market"}
          />
        </div>
        <div className="title">Your demat account is being created</div>
        <div className="content">
          You already started a demat account with us and its in progress,
          Please contact with us to make it faster
        </div>
        <Button
          onClick={() => {
            window.open(
              "https://wa.me/919847181078/?text=Hi, I would like to know more about breakout University.",
              "_blank"
            );
          }}
        >
          <div className="buttonWrapper">
            <Icons name="whatsapp" />
            <p className="text">Get Support</p>
          </div>
        </Button>
        <Button
          type="ghost"
          onClick={() => {
            window.location.href =
              "https://breakout.fundfolio.in/product/breakout-basics/";
          }}
        >
          Continue without offer
        </Button>
      </Container>
    </>
  );
};

export default OfferPage5;
