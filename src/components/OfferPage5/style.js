import styled from "styled-components";
import { rem } from "../../utils/remConverter";
export const Container = styled.div`
  padding: ${rem(40)};
  .content {
    color: #484848;
    font-family: gilroymedium;
    font-size: 16px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    margin-bottom: ${rem(24)};
    opacity: 0.6;
    text-align: left;
    @media (max-width: 767px) {
      font-size: 14px;
      font-weight: 500;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.5;
      letter-spacing: normal;
      text-align: center;
      color: #7f7f7f;
    }
  }
  .emojiDesktop {
    @media (max-width: 767px) {
      display: none;
    }
  }
  .emojiMobile{
    display: none;
    @media (max-width: 767px) {
      display: block;
    }
  }
  .iconWrapper {
    font-size: ${rem(100)};
    margin-bottom: ${rem(30)};
    @media (max-width: 767px) {
      font-size: ${rem(60)};
      margin-bottom: ${rem(24)};
    }
  }
  .title {
    color: #484848;
    font-family: gilroyextrabold;
    font-size: 36px;
    font-weight: 800;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.3;
    letter-spacing: -0.5px;
    margin-bottom: ${rem(16)};
    text-align: left;
    @media (max-width: 767px) {
      color: #484848;
      font-size: 28px;
      font-weight: 800;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.3;
      letter-spacing: -1px;
      text-align: center;
      margin-bottom: ${rem(12)};
    }
  }
  @media (max-width: 767px) {
    padding: ${rem(91)} ${rem(20)};
    text-align: center;
  }
`;

export const Button = styled.button`
  border: none;
  border-radius: 16px;
  background-color: ${(props) =>
    props.type === "ghost" ? "rgba(0,0,0,0)" : "#14cc60"};
  color: ${(props) => (props.type === "ghost" ? "#14cc60" : "#fff")};
  cursor: pointer;
  height: ${(props) => (props.type === "ghost" ? rem(20) : rem(54))};
  font-family: gilroybold;
  font-size: ${rem(18)};
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  margin-bottom: ${(props) => (props.type === "ghost" ? "0px" : rem(16))};
  letter-spacing: -0.5px;
  text-align: center;
  width: ${rem(440)};
  .buttonWrapper{
    display:flex;
    justify-content:center;
    align-items: center;
    .text{
      margin-left: ${rem(10)};
    }
  }
  @media (max-width: 1024px) {
    border-radius: 10px;
    height: ${rem(40)};
    font-size: ${rem(14)};
    width: ${rem(178)};
    padding: 0 0 2px 0;
  }
`;
