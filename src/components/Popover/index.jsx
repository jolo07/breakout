import React, { useState, useEffect } from "react";
import Icons from "../Icons";
import { Container } from "./style";
const Popover = (props) => {
  const [device, setDevice] = useState(true);
  useEffect(() => {
    if (window.innerWidth > 1025) {
      setDevice(true);
    } else {
      setDevice(false);
    }
  }, []);
  return (
    <Container>
      <div className="popover__wrapper">
        <div className="popover__title popover__container">
          <Icons name={props.name} width={device ? "26" : "20"} />
          <div className="popover__display">{props.text}</div>
        </div>
        <div className="popover__content">
          <p className="popover__message">{props.message}</p>
        </div>
      </div>
    </Container>
  );
};

export default Popover;
