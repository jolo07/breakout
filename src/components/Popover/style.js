import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Container = styled.div`
  margin-top: -4px;
  a {
    text-decoration: none;
  }

  .popover__title {
    font-size: 24px;
    line-height: normal;
    text-decoration: none;
    color: rgb(228, 68, 68);
    text-align: center;
    cursor: pointer;
  }

  .popover__container {
    display: flex;
    flex-direction: row;
    gap: 4px;
  }

  .popover__display {
    font-size: 14px;
    color: #0062ff;
  }

  .popover__wrapper {
    position: relative;
    display: inline-block;
  }
  .popover__content {
    opacity: 0;
    font-size: 10px;
    visibility: hidden;
    position: absolute;
    top: -75px;
    right: -68px;
    line-height: 1.25 !important;
    transform: translate(0, 10px);
    background-color: #fff;
    padding: ${rem(16)} ${rem(16)};
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
    border: 1px solid #eaeaea;
    border-radius: 10px;
    width: ${rem(165)};
    @media (min-width: 768px) {
      right: -65px;
    }
  }
  .popover__content:before {
    position: absolute;
    z-index: -1;
    content: "";
    right: calc(50% - 10px);
    top: 54px;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-top: 10px solid #fff;
    transition-duration: 0.3s;
    transition-property: transform;
  }
  .popover__wrapper:hover .popover__content {
    z-index: 10;
    opacity: 1;
    visibility: visible;
    transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
  }
  .popover__message {
    text-align: left;
  }
`;
