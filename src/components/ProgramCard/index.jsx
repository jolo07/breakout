import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import Icons from "../Icons";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import {
  Button,
  ProgramDetailsContainer,
  ProgramLanguage,
  MentorDescription,
  MentorTitleContainer,
  MentorName,
  MentorTitle,
  MentorDetailsContainer,
  BackgroundImageContainer,
  MentorDataContainer,
  ProgramContainer,
  ProgramDate,
  ProgramTitleContainer,
  ProgramTitle,
  ProgramPriceContainer,
  ProgramPrice,
  ProgramDuration,
  ProgramDescription,
  ProgramDescriptionContainer,
  MentorContactContainer,
  MentorContactDescription,
  SeatStockDetails,
  ProgramStatusContainer,
  EnrollmentDetails,
  GradientContainer,
} from "./style";
import { tracker } from "../../utils/tracker";
import { getProgramData } from "../../services/wooCommerce";
import { roundOffToNearestFifty } from "../../utils/helper";

Date.prototype.toShortFormat = function () {
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  let day = this.getDate();

  let monthIndex = this.getMonth();
  let monthName = monthNames[monthIndex];

  let year = this.getFullYear();

  return `${day} ${monthName} ${year}`;
};

const ProgramCardComponent = ({ programs, type, mockData }) => {
  const [smallDevice, setSmallDevice] = useState(false);
  const [outOfStock, setOutOfStock] = useState(false);
  const [availableSeats, setAvailableSeats] = useState();
  const [programDate, setProgramDate] = useState();
  const [mentorData, setMentorData] = useState(mockData.mentorData);
  const [programData, setProgramData] = useState(mockData.programData);
  const [programPrice, setProgramPrice] = useState();

  useEffect(async () => {
    const programDataFromWooCommerce = await getStockStatus();

    setMentorData(mockData.mentorData);
    setProgramData(mockData.programData);
    setProgramPrice(Number(programDataFromWooCommerce.price));
    setAvailableSeats(programDataFromWooCommerce.stock_quantity);

    const skuObject = JSON.parse(programDataFromWooCommerce.sku);
    const tempDate = new Date(skuObject.startDate);
    setProgramDate(tempDate.toShortFormat());
    if (window.innerWidth < 768) {
      setSmallDevice(true);
    } else {
      setSmallDevice(false);
    }
  }, [programs]);

  const getStockStatus = async () => {
    const data = await getProgramData(
      mockData.programData.programId,
      mockData.programData.variantId
    );
    return data;
  };

  return (
    <ProgramContainer type={type}>
      <MentorDataContainer>
        <GradientContainer />
        <BackgroundImageContainer>
          {smallDevice ? (
            <Image
              loader={gumletLoaderMarketfeedUniversity}
              src={mentorData.backgroundImage}
              width={266}
              height={200}
              objectFit="cover"
              alt="learn stock market trading"
            />
          ) : (
            <Image
              loader={gumletLoaderMarketfeedUniversity}
              className="bcgImage"
              src={mentorData.backgroundImage}
              width={400}
              height={400}
              objectFit="cover"
              alt={"learn stock market trading"}
            />
          )}
        </BackgroundImageContainer>
        <MentorDetailsContainer>
          <div className="mentorTitleContainer">
            <div className="mentorTitle">
              {mentorData?.mentorTitle ? mentorData?.mentorTitle : ""}
            </div>
            <div className="mentorName">
              {mentorData?.mentorName ? mentorData?.mentorName : ""}
            </div>
          </div>
          <div className="mentorDescription">
            {mentorData?.mentorDescription ? mentorData?.mentorDescription : ""}
          </div>
        </MentorDetailsContainer>
        <ProgramLanguage>
          {programData?.programLanguage ? (
            <div>{programData?.programLanguage}</div>
          ) : (
            ""
          )}
        </ProgramLanguage>
      </MentorDataContainer>
      <ProgramDetailsContainer>
        <ProgramDate>
          Next session on{" "}
          {/* {programData?.programDate ? programData?.programDate : ""} */}
          {programDate}
        </ProgramDate>
        <ProgramTitleContainer>
          <ProgramTitle>
            {programData?.programTitle ? programData?.programTitle : ""}
          </ProgramTitle>
          <ProgramPriceContainer>
            <ProgramPrice>
              <span style={{ fontSize: 12 }}>₹</span>
              {programPrice ? programPrice : programData?.programPrice}
            </ProgramPrice>
            <ProgramDuration>
              for{" "}
              {programData?.programDuration ? programData?.programDuration : ""}
            </ProgramDuration>
          </ProgramPriceContainer>
        </ProgramTitleContainer>
        <ProgramDescriptionContainer>
          {programData.programDescription.map((point) => {
            return (
              <span>
                <ProgramDescription>
                  <span className="icon">
                    <Icons name={"bulletPoint"} />
                  </span>
                  <p>{point}</p>
                </ProgramDescription>
              </span>
            );
          })}
        </ProgramDescriptionContainer>
      </ProgramDetailsContainer>

      <Link href={mentorData.mentorContactUrl}>
        <MentorContactContainer>
          <MentorContactDescription>
            Book a 30-Minutes Consultation
            <br />₹{" "}
            <span style={{ fontFamily: "gilroybold" }}>
              {mentorData.mentorContactPrice}
            </span>{" "}
            for <span style={{ fontFamily: "gilroybold" }}>30</span> Minutes
          </MentorContactDescription>
          <div className="mentorButton">
            <Icons name={"forwardIcon"} />
          </div>
        </MentorContactContainer>
      </Link>
      <ProgramStatusContainer>
        {programData.programEnrollment > 0 ? (
          <EnrollmentDetails>
            <Icons name={"seatIcon"} />
            <span className="value">
              {roundOffToNearestFifty(programData.programEnrollment)}+{" "}
            </span>{" "}
            Students Enrolled
          </EnrollmentDetails>
        ) : null}
          <SeatStockDetails visible={availableSeats <= 10}>
            <Icons name={"enrollmentIcon"} />
            Last<span className="value">&nbsp;{availableSeats} </span> Seats Left
          </SeatStockDetails>
      </ProgramStatusContainer>
      <div className="buttonWrapper">
        <Link href={mentorData.mentorContactUrl}>
          <Button type={availableSeats <= 0 ? "soldOut" : "inStock"}>
            {availableSeats > 10 ? "Learn More" : "Enroll Next Batch"}
          </Button>
        </Link>
      </div>
    </ProgramContainer>
  );
};

export default ProgramCardComponent;
