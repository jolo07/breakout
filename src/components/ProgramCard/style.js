import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Button = styled.button`
  border: ${(props) =>
    props.type === "inStock" ? "none" : "1px solid #14cc60"};
  border-radius: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) =>
    props.type === "inStock" ? "#14cc60" : "#fff"};
  color: ${(props) => (props.type === "inStock" ? "#fff" : "#14cc60")};
  cursor: pointer;
  height: ${rem(53)};
  font-family: gilroybold;
  font-size: ${rem(18)};
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.5px;
  padding: 0 0 2px 0;
  width: ${rem(322)};
  overflow: hidden;
  position: relative;
  -webkit-transition: all 0.25s;
  transition: all 0.25s;
  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    -webkit-transition: none;
    transition: none;
  }
  &:hover:before {
    width: 120%;
    background-color: rgba(255, 255, 255, 0);
    -webkit-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
  }
  @media (max-width: 1024px) {
    border-radius: 10px;
    height: ${rem(50)};
    font-size: ${rem(14)};
    width: ${rem(180)};
  }
`;

export const ProgramContainer = styled.div`
  background: ${(props) =>
    props.type === "primary"
      ? "none"
      : "linear-gradient(121.71deg, #F8FFDB -3.31%, #FBFADF 92.08%)"};
  background-color: ${(props) => (props.type === "primary" ? "#fff" : "none")};
  border: solid 1px rgba(15, 56, 73, 0.06);
  border-radius: 28px;
  // box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.04);
  display: flex;
  flex-direction: column;
  height: ${(props) => (props.type === "primary" ? rem(720) : rem(487))};
  // justify-content: space-between;
  margin-right: 20px;
  padding: ${rem(30)};
  position: relative;
  width: ${rem(364)};
  .buttonWrapper {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 20px;
    left: 20px;
    right: 20px;
    .communityButtonText {
      margin: 2px 0 0 0;
      @media (min-width: 768px) {
        margin: 0 0 0 4px;
      }
    }
    @media (max-width: 1024px) {
      position: absolute;
      bottom: 20px;
      left: 20px;
    }
  }
  // @media (min-width: 767px) and (max-width: 1023px) {
  //   margin-top:20px;
  // }
  @media (max-width: 1024px) {
    height: ${(props) => (props.type === "beginner" ? rem(336) : rem(532))};
    padding: ${rem(20)};
    margin-left: 20px;
    width: ${rem(268)};
  }
`;

export const MentorDataContainer = styled.div`
  border-radius: 28px;
  // background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%);
  display: flex;
  position: relative;
  height: 320px;
  width: ${rem(362)};
  margin: -30px;
  transition: all 0.4s ease-out;
  position: relative;
  padding-left: 30px;
  z-index: 100;
  ${ProgramContainer}:hover & {
    height: 465px;
    transition: all 0.4s ease-out;
    @media (max-width: 1024px) {
      height: 265px;
    }
  }
  @media (max-width: 1024px) {
    height: ${(props) => (props.type === "primary" ? rem(212) : "230px")};
    margin: -20px;
    padding-left: 20px;
    width: ${rem(266)};
  }
`;

export const GradientContainer = styled.div`
  background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%);
  height: 148px;
  width: 100%;
  left: 0;
  position: absolute;
  bottom: 0;
  z-index: 100;
  border-bottom-left-radius: 28px;
  border-bottom-right-radius: 28px;
  transition: all 0.4s ease-out;
  ${ProgramContainer}:hover & {
    transition: all 0.4s ease-out;
    bottom: 35px;
    background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%);
    border-bottom-left-radius: 28px;
    border-bottom-right-radius: 28px;
    height: 273px;
    width: 100%;
    transition: all 0.4s ease-out;
    @media (max-width: 1024px) {
      height: 150px;
      bottom: -25px;
    }
  }
  @media (max-width: 1024px) {
    height: 75px;
  }
`;

export const BackgroundImageContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: -100;
  background-size: cover;
  background-position: left;
  img {
    border-radius: 28px;
    width: 364px;
    height: 320px;
    transition: all 0.4s ease-out;
    overflow: hidden;
    object-position: center;
    ${ProgramContainer}:hover & {
      border-radius: 28px;
      width: 440px;
      height: 426px;
      overflow: hidden;
      object-position: top left;
      transition: all 0.4s ease-out;
      @media (max-width: 1024px) {
        height: 290px;
      }
    }
    @media (max-width: 1024px) {
      height: 225px;
    }
  }
`;

export const MentorDetailsContainer = styled.div`
  color: #fff;
  font-size: 22px;
  font-weight: 900;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.1px;
  text-align: left;
  z-index: 100;
  top: 250px;
  width: 100%;
  position: relative;
  height: 100px;
  .mentorTitleContainer {
    color: #fff;
    height: 20px;
    font-family: gilroymedium;
    font-size: 16px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.1px;
    margin-top: 25px;
    width: 100%;
    text-align: left;
    position: relative;
    @media (max-width: 1024px) {
      font-size: 12px;
      margin-top: 50px;
    }
  }
  .mentorName {
    font-family: gilroybold;
    font-size: 22px;
    font-weight: 900;
    max-width: 150px;
    position: absolute;
    bottom: 20px;
    @media (max-width: 1024px) {
      font-size: 16px;
      bottom: 16px;
    }
  }
  .mentorTitle {
    font-family: gilroymedium;
    font-size: 16px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.1px;
    text-align: left;
    color: #fff;
    opacity: 0.7;
    position: absolute;
    bottom: 0px;
    @media (max-width: 1024px) {
      font-size: 12px;
      bottom: 0px;
    }
  }
  .mentorDescription {
    font-size: 14px;
    opacity: 0;
    margin-top: 0px;
    padding-right: 20px;
    transition: all 0.4s ease-out;
    ${ProgramContainer}:hover & {
      margin-top: 16px;
      animation-name: slideDown;
      -webkit-animation-name: slideDown;
      animation-duration: 0.4s;
      -webkit-animation-duration: 0.4s;
      animation-timing-function: ease-out;
      -webkit-animation-timing-function: ease-out;
      opacity: 0.8;
      visibility: visible !important;
      @keyframes slideDown {
        0% {
          transform: translateY(-30%);
          opacity: 0;
        }
        100% {
          transform: translateY(0%);
          opacity: 0.8;
        }
      }

      @-webkit-keyframes slideDown {
        0% {
          -webkit-transform: translateY(-10%);
          opacity: 0;
        }
        100% {
          -webkit-transform: translateY(0%);
          opacity: 0.8;
        }
      }
      @media (max-width: 1024px) {
        margin-top: 6px;
      }
    }
    @media (max-width: 1024px) {
      font-size: 12px;
      margin-top: 4px;
      padding-right: 8px;
      color: rgba(255, 255, 255, 0.8);
    }
  }
  @media (max-width: 1024px) {
    font-size: 18px;
    top: 115px;
  }
`;

export const MentorTitleContainer = styled.div`
  color: #fff;
  height: 20px;
  font-family: gilroymedium;
  font-size: 16px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.1px;
  // margin-top: 267px;
  width: 100%;
  text-align: left;
  @media (max-width: 1024px) {
    font-size: 12px;
    margin-top: 50px;
  }
`;

export const ProgramLanguage = styled.div`
  color: #fddf43;
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  border: solid 1px rgba(255, 255, 255, 0.2);
  background-color: rgba(255, 255, 255, 0.2);
  font-family: gilroybold;
  font-size: 12px;
  font-weight: bold;
  font-stretch: normal;
  height: 31px;
  padding: 8px 16px;
  position: absolute;
  right: 0;
  top: 253px;
  white-space: nowrap;
  z-index: 100;
  @media (max-width: 1024px) {
    border: solid 1px rgba(255, 255, 255, 0.2);
    border-top-left-radius: 6px;
    border-bottom-left-radius: 6px;
    background-color: rgba(255, 255, 255, 0.2);
    font-size: 10px;
    height: 25px;
    font-weight: bold;
    font-stretch: normal;
    margin-top: 135px;
    padding: 6px 12px;
    margin-right: -1px;
    top: 15px;
  }
`;

export const ProgramDetailsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ProgramDate = styled.div`
  color: #5971f4;
  font-family: gilroymedium;
  font-size: 14px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.1px;
  // margin-top: 44px;
  position: absolute;
  top: 340px;
  text-align: left;
  transition: all 0.4s ease-out;
  ${ProgramContainer}:hover & {
    // opacity: 0;
    transition: all 0.4s ease-out 1s;
    transition-delay: 1ms;
  }
  @media (max-width: 1024px) {
    top: 210px;
    font-size: 12px;
    margin-top: 30px;
  }
`;

export const ProgramTitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  position: absolute;
  width: 300px;
  top: 370px;
  transition: all 0.4s ease-out;
  ${ProgramContainer}:hover & {
    top: 450px;
    transition: all 0.4s ease-out;
    @media (max-width: 1024px) {
      top: 243px;
      margin-top: 64px;
    }
  }
  @media (max-width: 1024px) {
    top: 250px;
    width: 228px;
    margin-top: 10px;
    margin-bottom: 15px;
  }
`;

export const ProgramTitle = styled.div`
  color: #353535;
  font-family: gilroybold;
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.5px;
  text-align: left;
  width: 70%;
  @media (max-width: 1024px) {
    font-size: 18px;
  }
`;

export const ProgramPriceContainer = styled.div`
  margin-top: 2px;
`;

export const ProgramPrice = styled.div`
  font-family: gilroybold;
  font-size: 20px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.5px;
  text-align: right;
  @media (max-width: 1024px) {
    font-size: 16px;
  }
`;

export const ProgramDuration = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.49;
  letter-spacing: normal;
  text-align: right;
  color: #767676;
  margin-top: 2px;
  @media (max-width: 1024px) {
    font-size: 12px;
  }
`;

export const ProgramDescription = styled.div`
  display: flex;
  font-family: gilroybold;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.2px;
  text-align: left;
  color: #484848;
  .icon {
    padding-top: 2px;
    margin-right: 10px;
    @media (max-width: 1024px) {
      margin-right: 8px;
    }
  }
  @media (max-width: 1024px) {
    font-size: 12px;
  }
`;

export const ProgramDescriptionContainer = styled.div`
  transition: all 0.4s ease-out;
  display: block;
  position: absolute;
  bottom: 200px;
  height: 65px;
  left: 35px;
  ${ProgramContainer}:hover & {
    opacity: 0;
    transition: all 0.4s ease-out;
    // display: none;
    @media (max-width: 1024px) {
    }
  }
  @media (max-width: 1024px) {
    bottom: 135px;
  }
`;

export const MentorContactContainer = styled.div`
  align-items: center;
  bottom: 122px;
  left: 20px;
  right: 20px;
  background-color: rgba(99, 123, 255, 0.06);
  border-radius: 18px;
  display: flex;
  flex-grow: 0;
  height: 68px;
  justify-content: space-between;
  padding: 16px;
  position: absolute;
  cursor: pointer;
  .mentorButton {
    cursor: pointer;
    padding-top: 4px;
  }
  @media (max-width: 1024px) {
    bottom: 95px;
    height: 48px;
    margin-bottom: 14px;
    padding: 12px;
    width: 228px;
  }
`;

export const MentorContactDescription = styled.div`
  width: 240px;
  font-family: gilroymedium;
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  text-align: left;
  color: #484848;
  .values {
    font-family: gilroymedium;
  }
  @media (max-width: 1024px) {
    font-size: 10px;
  }
`;

export const ProgramStatusContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  font-family: gilroymedium;
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.1px;
  text-align: left;
  color: #1c5992;
  position: absolute;
  bottom: 90px;
  left: 42px;
  right: 42px;
  .iconContainer {
    padding-right: 3px;
  }
  .value {
    font-family: gilroybold;
    margin-right: 2px;
  }
  @media (max-width: 1024px) {
    font-size: 10px;
    margin-bottom: 14px;
    bottom: 65px;
    left: 20px;
    right: 20px;
  }
`;

export const SeatStockDetails = styled.div`
  display: ${(props) => (props.visible ? "flex" : "none")};
  align-items: center;
`;

export const EnrollmentDetails = styled.div`
  align-items: center;
  display: flex;
`;
