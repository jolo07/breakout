import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { ScrollMenu } from "react-horizontal-scrolling-menu";
import Icons from "../Icons";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import Popover from "../Popover";
import ProgramCardComponent from "../ProgramCard";
import ConsultationCardComponent from "../ConsultationCard ";
import { getAllPrograms } from "../../services/wooCommerce";
import {
  Button,
  Container,
  CardContainer,
  CardSection,
  ProgramCard,
  StepsContainer,
  TradeLifeCycleBody,
  TradeLifeCycleWrapper,
  ProgramDescriptionContainer,
} from "./style";
import mockData from "./mockdata";
import { tracker } from "../../utils/tracker";
import usePreventBodyScroll from "../../utils/usePreventBodyScroll";

const ProgramsOffered = ({ openOfferModal, showLanguage }) => {
  const { disableScroll, enableScroll } = usePreventBodyScroll();

  const discordHandle = () => {
    tracker("join_community", { cta_from: "basics" });
    window.open("https://discord.gg/sf9g96x3N5", "_blank");
  };

  return (
    <Container id="programsOffered">
      <div className="backGroundSkew" />
      <div id="cardSectionContainer">
        <CardSection className="ProTraderCard">
          <div className="title">
            <h2 className="t2">
              Stock Market&nbsp;
              <span className="t1">Mentorship</span>
            </h2>
          </div>
        </CardSection>
        <div
          className="cardContainer"
          onMouseEnter={disableScroll}
          onMouseLeave={enableScroll}
        >
          <ScrollMenu>
            <div>
              <ProgramCardComponent
                type="primary"
                mockData={mockData.data[3]}
              />
            </div>
            <div>
              <ProgramCardComponent
                type="primary"
                mockData={mockData.data[4]}
              />
            </div>
          </ScrollMenu>
        </div>
        <div
          className="cardContainer"
          onMouseEnter={disableScroll}
          onMouseLeave={enableScroll}
        >
          <ScrollMenu>
            {" "}
            <div tabIndex={0}>
              <ProgramCardComponent
                type="primary"
                mockData={mockData.data[0]}
              />
            </div>{" "}
            <div tabIndex={0}>
              <ProgramCardComponent
                type="primary"
                mockData={mockData.data[1]}
              />
            </div>
          </ScrollMenu>
        </div>
      </div>
      <div id="cardSectionContainer">
        <CardSection className="ProTraderCard">
          <div className="title">
            <h2 className="t2">
             Talk to our tax and long term investing&nbsp;
              <span className="t1">Experts</span>
            </h2>
          </div>
        </CardSection>
        <div
          className="cardContainer"
          onMouseEnter={disableScroll}
          onMouseLeave={enableScroll}
        >
          <ScrollMenu>
            <div>
              <ConsultationCardComponent
                type="primary"
                mockData={mockData.data[5]}
              />
            </div>
            <div>
              <ConsultationCardComponent
                type="primary"
                mockData={mockData.data[6]}
              />
            </div>
          </ScrollMenu>
        </div>
      </div>
      <Link href="https://wa.me/919847181078/?text=Hi, I would like to know more on marketfeed.">
        <a target="_blank" rel="noreferrer noopener">
          <div
            style={{
              fontFamily: "gilroymedium",
              fontSize: "16px",
              lineHeight: "19px",
              color: "#484848",
              cursor: "pointer",
            }}
          >
            <span>Have any Queries?</span>
            <span
              style={{
                fontFamily: "gilroyextrabold",
              }}
            >
              &nbsp;Contact us
            </span>
          </div>
        </a>
      </Link>
      <TradeLifeCycleWrapper>
        <div className="tradeLifeCycleTitle">
          <h3 className="t1">
            We prepare you to succeed <br />
            in the <span className="t2"> Stock Market</span>
          </h3>
        </div>
        <TradeLifeCycleBody>
          <StepsContainer>
            <div className="stepDetails">
              <div className="stepHeader">STEP 1</div>
              <div className="stepTitle">
                <span className="t1">Join</span> us from anywhere, anytime.
              </div>
              <div className="stepContent">
                Marketfeed is easily accessible over the internet, allowing you
                to learn stock market trading and succeed wherever and whenever
                you want.
              </div>
            </div>
            <div className="imageWrapper">
              <Image
                loader={gumletLoaderMarketfeedUniversity}
                src={"/programsOffered1.png"}
                width={400}
                height={400}
                alt={"Best way to learn stock market"}
              />
            </div>
          </StepsContainer>
          <StepsContainer reverse={true}>
            <div className="stepDetails">
              <div className="stepHeader">STEP 2</div>
              <div className="stepTitle">
                <span className="t1">Learn</span> with live lessons and daily
                interactive Q&A sessions
              </div>
              <div className="stepContent">
                Access our expert-led daily sessions and interactive Q&A
                programs to make the journey easier in the stock market for
                beginners in India. Our all-inclusive and engaging online stock
                trading courses are designed to be the best way to learn stock
                market and get you ready to make profitable moves.
              </div>
            </div>
            <div className="imageWrapper">
              <Image
                loader={gumletLoaderMarketfeedUniversity}
                src={"/programsOffered5.png"}
                width={400}
                height={400}
                alt={"stock market for beginners in india"}
              />
            </div>
          </StepsContainer>
          <StepsContainer>
            <div className="stepDetails">
              <div className="stepHeader">STEP 3</div>
              <div className="stepTitle">
                <span className="t1">Trade</span> with live trading sessions led
                by expert traders and advisors
              </div>
              <div className="stepContent">
                Ever wondered how exactly people trade stocks in the real world
                and make money? Our stock market trading sessions will help you
                learn stock market trading with certain processes and give you
                the confidence to make your first trade!
              </div>
            </div>
            <div className="imageWrapper">
              <Image
                loader={gumletLoaderMarketfeedUniversity}
                src={"/programsOffered4.png"}
                width={400}
                height={400}
                alt={"learn stock market trading "}
              />
            </div>
          </StepsContainer>
          <StepsContainer reverse={true}>
            <div className="stepDetails">
              <div className="stepHeader">STEP 4</div>
              <div className="stepTitle">
                Losses, Experience and <span className="t1">Profits</span>{" "}
                #athishaktham 🚀
              </div>
              <div className="stepContent">
                The stock market for beginners in India is no rocket science.
                Still, it won’t be a smooth journey with instant returns. It is
                natural to make some losses, but we make sure that you learn
                from them and become successful traders just like us.
              </div>
            </div>
            <div className="imageWrapper">
              <Image
                loader={gumletLoaderMarketfeedUniversity}
                src={"/programsOffered3.png"}
                width={400}
                height={400}
                alt={"learn stock market trading"}
              />
            </div>
          </StepsContainer>
        </TradeLifeCycleBody>
      </TradeLifeCycleWrapper>
    </Container>
  );
};

export default ProgramsOffered;
