const mockData = {
  data: [
    {
      mentorData: {
        backgroundImage: "/abdul.png",
        mentorName: `Abdul${" "}Khader`,
        mentorTitle: "Active Option Seller",
        mentorDescription:
          "An active intraday equity trader and options seller, with a good knack for teaching the basics of the stock market to beginners. He usually makes monthly returns of 3 to 5%.",
        mentorContactDuration: 30,
        mentorContactPrice: 500,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktoabdul",
      },
      programData: {
        programLanguage: "MALAYALAM",
        programDate: "10 Nov 2021",
        programTitle: "Intraday & Weekly Options Selling",
        programPrice: 4999,
        programDuration: "1 Month",
        programDescription: [
          "Weekly Index Option Seller ",
          "Intraday Index Option Trader",
        ],
        programURL: "https://payments.marketfeed.com/product/abdul-khader/",
        programId: "25119",
        variantId: "25120",
        programEnrollment: 300,
        programSeatsLeft: "15",
      },
    },
    {
      mentorData: {
        backgroundImage: "/jain-new.png",
        mentorName: `Jain${" "}Jose`,
        mentorTitle: "The Basics Master",
        mentorDescription:
          "Jain has his strengths in Positional and Swing Trading. He is known for being a beloved teacher to a lot of stock market beginners. He usually makes monthly returns of 3 to 4%.",
        mentorContactDuration: 30,
        mentorContactPrice: 300,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktojain",
      },
      programData: {
        programLanguage: "MALAYALAM",
        programDate: "10 Nov 2021",
        programTitle: "Basics of Stock Market",
        programPrice: 7999,
        programDuration: "1 Month",
        programDescription: ["Positional Trader", "Swing Trader"],
        programURL: "https://payments.marketfeed.com/product/jain-jose/",
        programId: "22083",
        variantId: "24711",
        programEnrollment: 300,
        programSeatsLeft: "15",
      },
    },
    {
      mentorData: {
        backgroundImage: "/amal.png",
        mentorName: `Amal${" "}M${" "}Prashanth`,
        mentorTitle: "Active Option Seller",
        mentorDescription:
          "Amal follows a combination of conservative and aggressive strategies to make returns of 4-6% a month with the help of intraday option selling strangles and option selling spreads in weekly options.",
        mentorContactDuration: 30,
        mentorContactPrice: 300,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktoamal",
      },
      programData: {
        programLanguage: "MALAYALAM",
        programDate: "10 Nov 2021",
        programTitle: "Intraday & Weekly Options Selling",
        programPrice: 4999,
        programDuration: "1 Month",
        programDescription: [
          "Weekly Index Options Seller",
          "Intraday Index Option Trader",
        ],
        programURL: "https://payments.marketfeed.com/product/amal-prashanth/",
        programId: "25119",
        variantId: "25120",
        programEnrollment: 0,
        programSeatsLeft: "15",
      },
    },
    {
      mentorData: {
        backgroundImage: "/sharique.png",
        mentorName: `Sharique${" "}Samsudheen`,
        mentorTitle: "Pro Option Seller",
        mentorDescription:
          "He believes in making conservative returns but consistently, and thus he is a proud options seller targeting weekly expiries of NIFTY and BANKNIFTY. He usually makes monthly returns of 4 to 6%.",
        mentorContactDuration: 30,
        mentorContactPrice: 2000,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktosharique",
      },
      programData: {
        programLanguage: `ENGLISH`,
        programDate: "10 Nov 2021",
        programTitle: "Weekly Options Selling",
        programPrice: 19999,
        programDuration: "1 Month",
        programDescription: ["Youtube Fame", "Weekly Index Options Seller"],
        programURL:
          "https://payments.marketfeed.com/product/sharique-samsudheen/",
        programId: "17398",
        variantId: "24649",
        programEnrollment: 4000,
        programSeatsLeft: "15",
      },
    },
    {
      mentorData: {
        backgroundImage: "/ajay.png",
        mentorName: `Ajay${" "}Ajith`,
        mentorTitle: "Active Swing Trader",
        mentorDescription:
          "A Mathematics Graduate who found his niche in the stock market. Crunching numbers and analysing patterns is his favourite pastime. An avid trader in all areas of the market. He usually makes monthly returns of 4 to 5%.",
        mentorContactDuration: 30,
        mentorContactPrice: 500,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktoajay",
      },
      programData: {
        programLanguage: "MALAYALAM",
        programDate: "10 Nov 2021",
        programTitle: "Swing Trading & Intraday Futures",
        programPrice: 4999,
        programDuration: "1 Month",
        programDescription: ["Swing Trader", "Intraday Index Futures Trader"],
        programURL: "https://payments.marketfeed.com/product/ajay-ajith/",
        programId: "22417",
        variantId: "25118",
        programEnrollment: 600,
        programSeatsLeft: "15",
      },
    },
    {
      mentorData: {
        backgroundImage: "/sibi.png",
        mentorName: `Sibi${" "}Varghese`,
        mentorTitle: "Taxation Expert",
        mentorDescription:
          "Taxes are something everyone in our country is afraid of. Get the best advices on your Taxes, Audits and Accounts to stay on top of the game. Plan out your taxes and make sure you are not overpaying!",
        mentorContactDuration: 30,
        mentorContactPrice: 499,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktosibi",
      },
      programData: {
        programLanguage: "ENGLISH/MALAYALAM",
        programTitle: "Get all your Tax doubts cleared",
        programDescription: ["General Tax Consultation", "Tax Savings", "ITR"],
        variantId: "25118",
        mentorHighlight: { value: "200+", text: "Clientale" },
        programEnrollment: 300,
        experience: 10,
        highlightIcon: "clientIcon",
      },
    },
    {
      mentorData: {
        backgroundImage: "/vibin.png",
        mentorName: `Vibin${" "}Chittil`,
        mentorTitle: "Investments Expert",
        mentorDescription:
          "A Mutual Funds Specialist who has worked with top-tier firms in India. Designing and evaluating portfolios according to a client's personal goals is his expertise. Have helped more than 1000 clients take steps to reach their financial freedom.",
        mentorContactDuration: 30,
        mentorContactPrice: 299,
        mentorContactUrl:
          "https://mentorship.marketfeed.com/#/customer/talktovibin",
      },
      programData: {
        programLanguage: "ENGLISH/MALAYALAM",
        programTitle: "Swing Trading & Intraday Futures",
        programDescription: [
          "Long Term Investments",
          "General MF Consultation",
        ],
        mentorHighlight: { value: "10 Crores", text: "AUM Handled" },
        experience: 5,
        highlightIcon: "aumIcon",
      },
    },
  ],
};

export default mockData;

// const mockData = {
//   mentorData: {
//     backgroundImage: "tradeTogether/breakout1.png",
//     mentorName: "Sharique Samsudheen",
//     mentorTitle: "The basics Master",
//     mentorDescription:
//       "orem Ipsum is simply dummy text of the printing and typesetting industry",
//     mentorContactDuration: 30,
//     mentorContactPrice: 500,
//   },
//   programData: {
//     programLanguage: "English",
//     programDate: "10 Nov 2021",
//     programTitle: "Basics of Stock Market",
//     programPrice: 7000,
//     programDuration: "2 Months",
//     programDescription: ["Swing Trading", "Swing Trading", "Swing Trading"],
//     programURL: "/",
//     programId: "12312",
//     programEnrollment: 250,
//     programSeatsLeft: "15",
//   },
// };
