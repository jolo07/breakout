import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const Button = styled.button`
  border: ${(props) =>
    props.type === "secondary" ? "1px solid #5865F2" : "none"};
  border-radius: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) =>
    props.type === "secondary" ? "#fff" : "#14cc60"};
  color: ${(props) => (props.type === "secondary" ? "#5865F2" : "#fff")};
  cursor: pointer;
  height: ${rem(53)};
  font-family: gilroybold;
  font-size: ${rem(18)};
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.5px;
  padding: 0 0 2px 0;
  width: ${rem(304)};
  overflow: hidden;
  position: relative;
  -webkit-transition: all 0.5s;
  transition: all 0.5s;
  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    -webkit-transition: none;
    transition: none;
  }
  &:hover:before {
    width: 120%;
    background-color: rgba(255, 255, 255, 0);
    -webkit-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
  }
  @media (max-width: 1024px) {
    border-radius: 10px;
    height: ${rem(50)};
    font-size: ${rem(14)};
    width: ${rem(180)};
  }
`;
export const Container = styled.section`
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${rem(100)} ${rem(150)} ${rem(100)} ${rem(150)};
  position: relative;
  z-index: 0;
  @media (max-width: 767px) {
    padding: ${rem(0)} ${rem(30)};
  }
  @media (min-width: 767px) and (max-width: 1024px) {
    padding: ${rem(0)} ${rem(150)};
  }
  .backGroundSkew {
    position: absolute;
    top: 400px;
    left: 0;
    background-image: linear-gradient(
      to bottom,
      #e2f8ff,
      rgba(255, 255, 255, 0)
    );
    transform: skewY(-4deg);
    z-index: -1;
    width: 100vw;
    height: 100vh;
  }
  .cardContainer {
    width: 99vw;
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin-bottom: 30px;
    touch-action:pan-y;
    -webkit-overflow-scrolling: touch;
    @media (max-width: 767px) {
      display: block;
    }
  }
`;

export const CardSection = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  margin: 0 0 ${rem(60)} 0;
  justify-content: space-between;
  &.ProTraderCard {
    margin: ${rem(60)};
  }
  .title {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center:
    color: #484848;
    font-family: gilroyextrabold;
    font-size: 50px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: -1px;
    margin: 0 0 ${rem(5)} 0;
    text-align: center;
    z-index: 0;
    & .t1 {
      font-size: 50px;
      color: #637bff;
      @media (max-width: 1024px) {
        font-size: 32px;
      }
    }
    & .t2 {
      font-size: 50px;
      color: #484848;
      @media (max-width: 1024px) {
        font-size: 32px;
      }
    }
    @media (max-width: 768px) {
      width: ${rem(320)};
    }
    @media (max-width: 1024px) {
      font-size: 32px;
      font-weight: 800;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.2;
      letter-spacing: -1px;
      margin-right: ${rem(0)};
      text-align: center;
      color: #484848;
    }
  }
  @media (max-width: 1024px) {
    margin: ${rem(60)} 0 ${rem(60)} 0;
    &.ProTraderCard {
      margin: ${rem(60)} 0;
    }
  }
  @media (max-width: 1024px) {
    flex-direction: column;
  }
`;

export const CardContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 36px;
  padding-left: 20px;
  @media (max-width: 769px) {
    padding-left: 20px;
    width: 100vw;
    overflow-x: hidden;
    padding-bottom: 20px;
  }
  @media (min-width: 767px) and (max-width: 1023px) {
    justify-content: space-around;
    overflow: hidden;
    flex-wrap: wrap;
    padding-bottom: 20px;
  }
`;

export const ProgramCard = styled.div`
  background: ${(props) =>
    props.type === "beginner"
      ? "none"
      : "linear-gradient(121.71deg, #F8FFDB -3.31%, #FBFADF 92.08%)"};
  background-color: ${(props) => (props.type === "beginner" ? "#fff" : "none")};
  border: solid 1px rgba(15, 56, 73, 0.06);
  border-radius: 20px;
  // box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.04);
  display: flex;
  flex-direction: column;
  height: ${(props) => (props.type === "beginner" ? rem(382) : rem(487))};
  justify-content: space-between;
  margin-right: 20px;
  padding: ${rem(30)};
  position: relative;
  width: ${rem(364)};
  .buttonWrapper {
    display: flex;
    justify-content: center;
    align-items: center;
    .communityButtonText {
      margin: 2px 0 0 0;
      @media (min-width: 768px) {
        margin: 0 0 0 4px;
      }
    }
  }
  .cardDescription {
    font-family: gilroymedium;
    font-size: 16px;
    line-height: 21px;
    margin-bottom: 16px;
    color: rgb(72, 72, 72, 0.8);
    .bold {
      font-family: gilroybold;
    }
    @media (max-width: 1024px) {
      font-size: ${rem(14)};
      margin-bottom: 8px;
      line-height: 21px;
    }
  }
  .cardRibbonWrapper {
    position: absolute;
    top: 55px;
    right: 0px;
    @media (min-width: 767px) {
      position: absolute;
      top: 76px;
      right: 0px;
    }
  }
  .featureList {
    line-height: 30px;
    color: #484848;
    font-family: gilroybold;
    .feature {
      font-size: ${rem(16)};
      @media (max-width: 1024px) {
        font-size: ${rem(14)};
      }
    }
    @media (max-width: 1024px) {
      font-size: ${rem(12)};
      line-height: 2.14;
    }
  }
  .price {
    font-size: ${rem(24)};
    font-family: gilroyextrabold;
    margin-bottom: ${rem(20)};
    color: #484848;
    .rupee {
      font-family: gilroymedium;
      font-size: ${rem(16)};
      @media (max-width: 1024px) {
        font-size: ${rem(12)};
        margin-bottom: ${rem(8)};
      }
    }
    .seat {
      font-size: ${rem(16)};
      @media (max-width: 1024px) {
        font-size: ${rem(12)};
        margin-bottom: ${rem(8)};
      }
    }
    @media (max-width: 1024px) {
      font-size: ${rem(20)};
      margin-bottom: ${rem(22)};
    }
  }
  .offerInnerCard {
    align-items: center;
    display: ${(props) => (props.offer ? "block" : "none")};
    font-size: 14px;
    padding: 14px 16px;
    margin-top: ${rem(12)};
    margin-bottom: ${rem(20)};
    position: relative;
    z-index: 4;
    .text {
      display: flex;
      justify-content: space-between;
      color: #484848;
      .t1 {
        font-family: gilroybold;
        color: #637bff;
      }
      .modalTrigger {
        color: #484848;
        cursor: pointer;
        font-family: gilroybold;
      }
      @media (max-width: 1024px) {
        font-family: gilroymedium;
        font-size: 10px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: -0.5px;
        text-align: left;
      }
    }
    .bg {
      position: absolute;
      border-radius: 6px;
      z-index: -1;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-image: linear-gradient(to top, #eade71, #ffd3ba);
      opacity: 0.2;
      width: 100%;
      height: 100%;
    }
    @media (max-width: 1024px) {
      padding: ${rem(12)} ${rem(16)};
      margin-top: ${rem(12)};
      display: block;
    }
  }
  .cardTitle {
    display: flex;
    align-items: center;
    font-family: gilroyextrabold;
    color: #8149a8;
    font-size: ${rem(30)};
    letter-spacing: -0.5px;
    margin-bottom: ${rem(4)};
    @media (max-width: 1024px) {
      font-size: ${rem(20)};
      margin-bottom: ${rem(6)};
    }
  }
  @media (max-width: 1024px) {
    box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.06);
    border: solid 1px rgba(15, 56, 73, 0.06);
    height: ${(props) => (props.type === "beginner" ? rem(336) : rem(432))};
    padding: ${rem(20)};
    width: ${rem(270)};
  }
`;
export const StepsContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: ${(props) => (props.reverse ? "row-reverse" : "row")};
  justify-content: space-around;
  .stepDetails {
    color: rgba(72, 72, 72, 0.8);
    font-family: gilroymedium;
    font-size: ${rem(16)};
    line-height: 1.5;
    text-align: left;
    width: ${rem(502)};
    @media (max-width: 1024px) {
      font-size: ${rem(14)};
      width: 100%;
      text-align: center;
    }
  }
  .stepHeader {
    color: #8149a8;
    font-family: gilroybold;
    letter-spacing: 2px;
    font-size: ${rem(14)};
    margin-bottom: ${rem(12)};
    @media (max-width: 1024px) {
      font-size: ${rem(12)};
      margin-bottom: ${rem(8)};
    }
  }
  .stepTitle {
    color: #484848;
    font-family: gilroymedium;
    font-size: 30px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: -0.5px;
    margin-bottom: ${rem(12)};
    text-align: left;
    width: ${rem(417)};
    .t1 {
      font-family: gilroyextrabold;
      color: #637bff;
    }
    @media (max-width: 1024px) {
      font-size: ${rem(20)};
      font-stretch: normal;
      font-style: normal;
      line-height: 1.2;
      letter-spacing: -0.5px;
      padding: 0 ${rem(19)};
      text-align: center;
      color: #484848;
      width: 100%;
    }
  }
  @media (max-width: 1024px) {
    flex-direction: column;
    margin-bottom: 60px;
    text-align: center;
    width: 100%;
  }
`;
export const TradeLifeCycleBody = styled.div`
  width: 100%;
`;
export const TradeLifeCycleWrapper = styled.div`
  max-width: ${rem(900)};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  .tradeLifeCycleTitle {
    width: ${rem(720)};
    color: #484848;
    letter-spacing: -1px;
    line-height: 1.2;
    margin: ${rem(100)} 0 ${rem(60)} 0;
    padding: 0 ${rem(50)};
    text-align: center;
    & .t1 {
      font-family: gilroyextrabold;
      font-size: 50px;
      color: #484848;
      @media (max-width: 1024px) {
        font-size: 32px;
      }
    }
    & .t2 {
      font-family: gilroyextrabold;
      font-size: 50px;
      color: #637bff;
      @media (max-width: 1024px) {
        font-size: 32px;
      }
    }
    @media (max-width: 1024px) {
      width: ${rem(320)};
      color: #484848;
      font-size: 32px;
      font-weight: 800;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.2;
      letter-spacing: -1px;
      margin: 0 0 ${rem(40)} 0;
      padding: 0 ${rem(22)};
      text-align: center;
    }
  }
`;

export const ProgramDescriptionContainer = styled.div`
  display: flex;
  font-family: gilroymedium;
  font-size: 20px;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin-top: 12px;
  .contactText {
    text-align: center;
    width: 275px;
    font-family: gilroymedium;
    font-size: 14px;
    font-stretch: normal;
    font-weight: 500;
    font-style: normal;
    color: #484848cc;
    line-height: 1.25;
    letter-spacing: normal;
  }
  @media (min-width: 768px) {
    .contactText {
      text-align: center;
      width: ${rem(502)};
    }
  }
`;
