import React from "react";
import "react-toastify/dist/ReactToastify.css";

import { Container } from "./style";
const Spinner = ({ active }) => {
  return (
    <>
      <Container active={active}>
        <div id="loading"></div>
      </Container>
    </>
  );
};

export default Spinner;
