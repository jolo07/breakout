import React from "react";
import Image from "next/image";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import Icons from "../Icons/index.jsx";
import {
  Container,
  Title,
  ContactCard,
  ImageWrapper,
  Button,
  Section,
} from "./style";
import Link from "next/link";
import { FaWhatsapp } from "react-icons/fa";
import { tracker } from "../../utils/tracker";

const TalkToUs = () => (
  <Section>
    <Container>
      <div className="bg"></div>
      <div className="overlay">
        <Icons name="talkToUsOverlay" />
      </div>
      <div className="imageOverlayDesktop">
        <Image
          loader={gumletLoaderMarketfeedUniversity}
          src={"/talkToUs.png"}
          width={629}
          height={492}
          alt={"Best way to learn stock market"}
        />
      </div>
      <div className="imageOverlayMobile">
        <Image
          loader={gumletLoaderMarketfeedUniversity}
          src={"/talkToUsMobile.png"}
          width={1280}
          height={2356}
          alt={"Best way to learn stock market"}
        />
      </div>
      <Title>Talk to Us</Title>
      <div className="wrapper">
        <ContactCard>
          <span className="containerDivider"></span>
          <div className="contactSection1">
            <div className="sectionTitle">Send us an Email</div>
            <div className="sectionDetail">
              <a className="link" href="mailto:marketfeed@fundfolio.in">
                marketfeed@fundfolio.in
              </a>
            </div>
          </div>
          <div className="contactSection2">
            <div className="sectionDetail">
              <Link href="https://wa.me/919847181078/?text=Hi, I would like to know more on marketfeed.">
                <a
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    textDecoration: "none",
                  }}
                >
                  <Button
                    onClick={() => {
                      tracker("whatsapp_cta", {
                        cta_from: "talk to us",
                      });
                    }}
                  >
                    <FaWhatsapp className="my-float ml-4" />
                    &nbsp;Chat with us
                  </Button>
                </a>
              </Link>
            </div>
          </div>
        </ContactCard>
        <ImageWrapper />
      </div>
    </Container>
  </Section>
);

export default TalkToUs;
