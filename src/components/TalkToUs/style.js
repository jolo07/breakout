import styled from "styled-components";
import { rem } from "../../utils/remConverter";

export const ContactCard = styled.div`
  background-color: #ffffff;
  border-radius: 40px;
  border: solid 1px rgba(0, 0, 0, 0.04);
  display: flex;
  justify-content: space-between;
  flex-grow: 0;
  height: ${rem(213)};
  margin: 40px 0 0;
  padding: ${rem(60)} ${rem(66)} ${rem(60)} ${rem(65)};
  position: relative;
  width: 754px;
  z-index: 1;
  .containerDivider {
    position: absolute;
    top: 50%;
    right: 45%;
    transform: translate(50%, -50%);
    width: 1px;
    height: 90px;
    border: 1px solid rgba(0, 0, 0, 0.04);
    @media (max-width: 1024px) {
      display: none;
    }
  }
  .contactSection1 {
    padding-right: 30px;
    .sectionTitle {
      color: #8149a8;
      opacity: 0.7;
      font-family: gilroybold;
      font-size: 22px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: -0.5px;
      text-align: left;
      color: #8149a8;
      @media (max-width: 1024px) {
        font-size: 16px;
        text-align: center;
      }
    }
    .sectionDetail {
      cursor: pointer;
      font-family: gilroyextrabold;
      font-size: 30px;
      line-height: 1.67;
      letter-spacing: -1px;
      text-align: left;
      .link {
        color: #484848;
        text-decoration: none;
      }
      @media (max-width: 1024px) {
        font-size: 24px;
      }
    }
    @media (max-width: 1024px) {
      padding: 0;
      border: none;
      padding-bottom: 20px;
      border-bottom: 1px solid rgba(0, 0, 0, 0.04);
    }
  }
  .contactSection2 {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    .sectionTitle {
      color: #8149a8;
      opacity: 0.7;
      font-family: gilroybold;
      font-size: 22px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: -0.5px;
      text-align: left;
      color: #8149a8;
      @media (max-width: 1024px) {
        font-size: 16px;
        text-align: center;
      }
    }
    .sectionDetail {
      font-family: gilroyextrabold;
      font-size: 30px;
      line-height: 1.67;
      letter-spacing: -1px;
      text-align: left;
      .link {
        color: #484848;
        text-decoration: none;
      }
      @media (max-width: 1024px) {
        font-size: 24px;
      }
    }
  }
  @media (max-width: 1024px) {
    align-items: center;
    height: ${rem(232)};
    flex-direction: column;
    padding: ${rem(40)};
    width: ${rem(280)};
  }
`;

export const Section = styled.div`
  display: flex;
  width: 100vw;
  justify-content: center;
  align-items: center;
  background-color: rgb(160, 187, 255, 0.1);
  `;
  
export const Container = styled.div`
  align-items: flex-start;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  padding: ${rem(150)} ${rem(150)};
  overflow: hidden;
  position: relative;
  .bg {
    background-color: #a0bbff;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0.1;
    width: 100%;
  }
  .imageOverlayDesktop {
    bottom: -10px;
    right: 0;
    position: absolute;
    object-fit: contain;
    width: ${rem(629)};
    z-index: 0;
    position: absolute;
    object-fit: contain;
    @media (max-width: 767px) {
      display: none;
    }
  }
  .imageOverlayMobile {
    display: none;
    @media (max-width: 767px) {
      display: flex;
      bottom: 0;
      right: 0;
      position: absolute;
      object-fit: contain;
    }
  }
  .overlay {
    bottom: -600px;
    right: 0;
    position: absolute;
    object-fit: contain;
    z-index: 0;
    @media (max-width: 1024px) {
      display: none;
    }
  }
  .wrapper {
    display: flex;
    @media (max-width: 1024px) {
      align-items: center;
      flex-direction: column;
    }
  }
  @media (max-width: 1024px) {
    padding: 0;
    width: 100%;
    align-items: center;
  }
  @media (min-width: 1280px) {
    width: 100%;
    padding: ${rem(100)} ${rem(150)};
  }
  @media (min-width: 1980px) {
    width: ${rem(1980)};
  }
`;
export const ImageWrapper = styled.div`
  height: 305px;
`;
export const Title = styled.div`
  color: #484848;
  font-family: gilroyextrabold;
  font-size: 50px;
  font-weight: 800;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: -1px;
  text-align: left;
  @media (max-width: 1025px) {
    font-size: ${rem(32)};
    line-height: 1.2;
    padding-top: ${rem(60)};
    text-align: center;
  }
`;

export const Button = styled.div`
  width: ${rem(127)};
  height: ${rem(40)};
  flex-grow: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  font-family: gilroybold;
  text-transform: none;
  letter-spacing: 0px;
  box-shadow: 0 4px 18px 0 rgba(0, 0, 0, 0.09);
  border: solid 1px rgba(255, 255, 255, 0.1);
  background-color: #14cc60;
  color: #ffff;
  font-size: 12px;
  cursor: pointer;
  overflow: hidden;
  position: relative;
  -webkit-transition: all 0.5s;
  transition: all 0.5s;
  &:before {
    content: "";
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    -webkit-transition: none;
    transition: none;
  }
  &:hover:before {
    width: 120%;
    background-color: rgba(255, 255, 255, 0);
    -webkit-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
  }
  @media (min-width: 768px) {
    width: ${rem(210)};
    height: ${rem(53)};
    font-size: 18px;
    padding: 0 0 2px 0;
  }
  @media (min-width: 1280px) {
    border-radius: 16px;
    font-size: ${rem(18, "medium")};
  }
  @media (min-width: 1440px) {
    font-size: ${rem(18)};
  }
`;
