import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useSpring, animated, useTransition } from "react-spring";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import {
  ActiveTabs,
  Container,
  FeatureImage,
  FeatureImageWrapper,
  Section,
  Tab,
  Column,
  Row,
  P,
  H1,
  AnimateText,
  AnimateImage,
  AnimateImage2,
} from "./style";

const cards = [
  "/assets/AnimOne.json",
  "/assets/AnimTwo.json",
  "/assets/AnimThree.json",
  "/assets/AnimFour.json",
];

const cardsMob = [
  "/assets/AnimOneMob.json",
  "/assets/AnimTwoMob.json",
  "/assets/AnimThreeMob.json",
  "/assets/AnimFourMob.json",
];

const TradeTogether = () => {
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [startAnimation, setStartAnimation] = useState(false);
  const [device, setDevice] = useState();
  const [timerId, setTimerId] = useState();
  const [reverse, setReverse] = useState(false);
  const [feature, setFeature] = useState("lottieAnimation");
  const [isMobileView, setIsMobileView] = useState(false);
  useEffect(() => {
    if (window.innerWidth > 1024) {
      setDevice(true);
      setFeature("featureSection");
    } else if (window.innerWidth < 768) {
      setIsMobileView(true);
    } else {
      setIsMobileView(false);
      setDevice(false);
      setFeature("lottieAnimation");
    }
  }, []);

  useEffect(() => {
    const timerId = setInterval(() => {
      if (startAnimation) {
        setActiveTabIndex((state) => (state + 1) % cards.length);
      }
    }, 4000);
    setTimerId(timerId);
    return () => clearTimeout(timerId);
  }, [activeTabIndex, startAnimation]);

  useEffect(() => {
    const featureSection = document.getElementById(feature);
    document.addEventListener(
      "scroll",
      function () {
        if (!startAnimation && featureSection !== null) {
          setStartAnimation(isInViewport(featureSection));
        }
      },
      {
        passive: true,
      }
    );
    document.addEventListener("swiped-left", function () {
      nextLottie();
    });

    document.addEventListener("swiped-right", function () {
      previousLottie();
    });
  });

  useEffect(() => {
    if (!startAnimation) {
      activeTabAnimationApi.start({
        to: { opacity: 1, transform: "translateY(0px)" },
        from: { opacity: 0, transform: "translateY(10px)" },
        config: { duration: 500 },
      });
    }
  }, [startAnimation]);

  const transitionDesktop = useTransition(activeTabIndex, {
    key: activeTabIndex,
    from: {
      opacity: 0,
      transform: reverse ? `translateY(-100%)` : `translateY(100%)`,
    },
    enter: { opacity: 1, transform: `translateY(10%)` },
    leave: {
      opacity: 0,
      transform: `translateY(100%)`,
    },
  });

  const transitionMobile = useTransition(activeTabIndex, {
    key: activeTabIndex,
    from: {
      opacity: 0,
      transform: reverse ? `translateX(-100%)` : `translateX(100%)`,
    },
    enter: { opacity: 1, transform: `translateX(0%)` },
    leave: {
      opacity: 0,
      transform: `translateX(100%)`,
    },
  });

  const nextLottie = () => {
    handleTabSwitch(activeTabIndex + 1);
  };

  const previousLottie = () => {
    handleTabSwitch(activeTabIndex - 1);
  };

  // Animations
  const [activeTabAnimation, activeTabAnimationApi] = useSpring(() => ({
    to: { opacity: 1, transform: "translateY(0px)" },
    from: { opacity: 0, transform: "translateY(20px)" },
  }));

  // Check to see if slide is in view
  const isInViewport = (element) => {
    const rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <=
        (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  };

  const handleTabSwitch = (tabId) => {
    clearTimeout(timerId);
    if (tabId < 0) tabId = 0;
    if (tabId > 3) tabId = 3;
    tabId = tabId % cards.length;
    if (activeTabIndex > tabId) {
      setReverse(true);
    } else {
      setReverse(false);
    }
    switch (tabId) {
      case 0:
        setActiveTabIndex(0);
        break;

      case 1:
        setActiveTabIndex(1);
        break;

      case 2:
        setActiveTabIndex(2);
        break;

      case 3:
        setActiveTabIndex(3);
        break;

      default:
        break;
    }
  };
  return (
    <Section>
      <Container>
        <ActiveTabs className="tabContainer" style={activeTabAnimation}>
          <div
            style={{
              position: "relative",
              display: "flex",
              flexDirection: `${device ? "column" : "row"}`,
            }}
          >
            <Tab
              active={activeTabIndex === 0}
              className={device ? "one" : "none"}
              onClick={() => handleTabSwitch(0)}
            />
            <Tab
              active={activeTabIndex === 1}
              onClick={() => handleTabSwitch(1)}
              className={device ? "two" : "none"}
            />
            <Tab
              active={activeTabIndex === 2}
              onClick={() => handleTabSwitch(2)}
              className={device ? "three" : "none"}
            />
            <Tab
              active={activeTabIndex === 3}
              onClick={() => handleTabSwitch(3)}
              className={device ? "four" : "none"}
            />
          </div>
        </ActiveTabs>

        <div className="contentWrapper">
          <Column>
            <div className="backArrow" onClick={() => previousLottie()}>
              <IoIosArrowBack />
            </div>
            <FeatureImageWrapper
              className={activeTabIndex === 0 && "active"}
              id="lottieAnimation"
            >
              <AnimateText
                className={`textAnimation ${activeTabIndex === 0 && "active"}`}
              >
                Learn Live with Top Traders{" "}
              </AnimateText>
              <AnimateImage
                className={`firstImage ${activeTabIndex === 0 && "active"}`}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout1.png"
                  width={240}
                  height={292}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage>
              <AnimateImage2
                className={`secondImage  ${activeTabIndex === 0 && "active"}`}
                width={175}
                right={isMobileView ? 160 : 120}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout2.png"
                  width={175}
                  height={115}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage2>
            </FeatureImageWrapper>
            <FeatureImageWrapper
              className={activeTabIndex === 1 && "active"}
              id="lottieAnimation"
            >
              <AnimateText
                className={`textAnimation ${activeTabIndex === 1 && "active"}`}
              >
                Live Audio <br /> Rooms{" "}
              </AnimateText>
              <AnimateImage
                className={`firstImage ${activeTabIndex === 1 && "active"}`}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout3.png"
                  width={240}
                  height={292}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage>
              <AnimateImage2
                className={`secondImage  ${activeTabIndex === 1 && "active"}`}
                width={116}
                right={180}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout4.png"
                  width={116}
                  height={128}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage2>
            </FeatureImageWrapper>
            <FeatureImageWrapper
              className={activeTabIndex === 2 && "active"}
              id="lottieAnimation"
            >
              <AnimateText
                className={`textAnimation ${activeTabIndex === 2 && "active"}`}
              >
                Trades & Analysis from Experts{" "}
              </AnimateText>
              <AnimateImage
                className={`firstImage ${activeTabIndex === 2 && "active"}`}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout5.png"
                  width={240}
                  height={292}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage>
              <AnimateImage2
                className={`secondImage  ${activeTabIndex === 2 && "active"}`}
                width={220}
                right={isMobileView ? 160 : 120}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout6.png"
                  width={220}
                  height={151}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage2>
            </FeatureImageWrapper>
            <FeatureImageWrapper
              className={activeTabIndex === 3 && "active"}
              id="lottieAnimation"
            >
              <AnimateText
                className={`textAnimation ${activeTabIndex === 3 && "active"}`}
              >
                Chat with Community{" "}
              </AnimateText>
              <AnimateImage
                className={`firstImage ${activeTabIndex === 3 && "active"}`}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout7.png"
                  width={240}
                  height={292}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage>
              <AnimateImage2
                className={`secondImage  ${activeTabIndex === 3 && "active"}`}
                width={260}
                right={isMobileView ? 140 : 0}
              >
                <Image
                  className="test"
                  src="/tradeTogether/breakout8.png"
                  width={260}
                  height={153}
                  loader={gumletLoaderMarketfeedUniversity}
                />
              </AnimateImage2>
            </FeatureImageWrapper>
            <div className="forwardArrow" onClick={() => nextLottie()}>
              <IoIosArrowForward />
            </div>
          </Column>
          <Column id="featureSection" className="contentSection">
            <H1>
              <span style={{ color: "#637BFF" }}> Learn & Trade</span>
              <br></br>Together
            </H1>
            <P>
              Community based
              <span style={{ fontFamily: "gilroyextrabold" }}>
                &nbsp; Learning Programs , Virtual Trading Rooms&nbsp;and
                Personal Mentorship&nbsp;
              </span>
              run by successful traders.
            </P>
            <Row className="content1">
              <Image
                loader={gumletLoaderMarketfeedUniversity}
                src="/thumbsup.png"
                width={80}
                height={80}
                alt={"thumbsup"}
              />
              <P className="capital margin">
                Hassle-free
                <br /> learning programme
              </P>
            </Row>
          </Column>
        </div>
      </Container>
    </Section>
  );
};

export default TradeTogether;
