import styled from "styled-components";
import { rem } from "../../utils/remConverter";
import { animated } from "react-spring";

export const ActiveTabs = styled(animated.div)`
  display: flex;
  z-index: 1;
  @media (width: 1024px) {
    flex-direction: column;
  }
  @media (min-width: 1025px) {
    margin-bottom: 45px;
    width: ${rem(30)};
    flex-direction: column;
  }
`;

export const Tab = styled.div`
  border: 2px solid #FFC400;
  background-color: #ffc400;
  letter-spacing: -1px;
  opacity: ${(props) => (props.active ? "1" : "0.4")};
  margin: ${rem(10)};
  border-radius: 3px;
  text-align: left;
  animation: ${(props) => (props.active ? "slidein" : "none")} 0.5s ease-in;
  width: ${(props) => (props.active ? "28px" : "10px")};
  cursor: pointer;
  &.one {
    position: absolute;
    top: 0;
    right: 100px;
  }
  &.two {
    position: absolute;
    top: 20px;
    right: 100px;
  }
  &.three {
    position: absolute;
    top: 40px;
    right: 100px;
  }
  &.four {
    position: absolute;
    top: 60px;
    right: 100px;
  }
  @keyframes slidein {
    0% {
      opacity: 0.2;
      width: 10px;
    },
    100% {
      opacity: 1;
      width: 28px;
    }
  }
  }
  @media (max-width: 767px) {
    font-size: 16px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1;
    letter-spacing: -0.5px;
    text-align: left;
    color: #9ea0a4;
  }
  @media (min-width: 768px) {
    font-size: 18px;
  }
  @media (min-width: 1280px) {
    font-size: 24px;
  }
`;

export const Container = styled(animated.div)`
  align-items: center;
  background-color: #ffffff;
  display: flex;
  flex-direction: column-reverse;
  height: ${rem(860)};
  padding: ${rem(0)} ${rem(248)};
  overflow: hidden;
  position: relative;
  width: 100%;
  .backArrow {
    display: block;
    position: absolute;
    top: 50%;
    left: -5%;
    @media (min-width: 768px) {
      display: none;
    }
  }
  .forwardArrow {
    display: block;
    position: absolute;
    top: 50%;
    left: 100%;
    @media (min-width: 768px) {
      display: none;
    }
  }
  .backgroundImage {
    position: absolute;
    bottom: 50px;
    right: 0;
    @media (max-width: 1279px) {
      display: none;
    }
    @media (min-width: 1280px) {
      display: block;
    }
  }
  .backgroundImageMobile {
    @media (max-width: 767px) {
      display: block;
      bottom: 150px;
      position: absolute;
      right: 0;
    }
    @media (min-width: 767px) {
      display: block;
      bottom: 250px;
      position: absolute;
      right: 0;
    }
    @media (min-width: 1280px) {
      display: none;
    }
  }
  .contentWrapper {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    @media (max-width: 1024px) {
      align-items: center;
      flex-direction: column-reverse;
    }
    @media (max-width: 767px) {
      height: 100%;
    }
  }
  .wrapper {
    display: flex;
    @media (max-width: 1024px) {
      align-items: center;
      flex-direction: column;
    }
  }
  @media (max-width: 767px) {
    padding: 0 16px;
    align-items: center;
    justify-content: flex-end;
  }
  @media (max-width: 1024px) and (min-width: 768px) {
    padding: 0;
    margin: 0 0 3rem 0;
  }
  @media (min-width: 1025px) {
    height: ${rem(640)};
    padding: 80px 75px;
    flex-direction: row;
  }
  @media (min-width: 1280px) {
    align-items: center;
    padding: 80px ${rem(204)} 0 ${rem(204)};
  }
  @media (min-width: 1440px) {
    padding: 80px ${rem(268)} 0 ${rem(268)};
  }
  @media (min-width: 1980px) {
    width: ${rem(1440)};
  }
`;

export const P = styled.div`
  font-family: gilroymedium;
  font-size: ${rem(14)};
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: normal;
  text-align: center;
  color: #484848CC;
  &.capital {
    text-transform: uppercase;
    color: #6e7176
    font-size: ${rem(12)};
  }
  &.margin {
    margin: 0 0 0 ${rem(20)};
    text-align: left;
    color: #484848;
    font-family: gilroyregular;
    line-height: 26px;
    letter-spacing: 2px;
  }
  @media (min-width: 1025px) {
    text-align: left;
    width: ${rem(351)};
    font-size: ${rem(16)};
  }
  `;

export const FeatureImageWrapper = styled(animated.div)`
  position: absolute;
  left: 0;
  width: 500px;
  opacity: 0;
  @media (max-width: 768px) {
    top: 0;
  }

  &.active {
    opacity: 1;
  }
  .textAnimation {
    transform: translate(0px, 100px);
    opacity: 0;
    transition: all 1s ease;
    @media (max-width: 768px) {
      transform: translate(100px, 0px);
    }
    &.active {
      transform: translate(0px, 0px);
      opacity: 0.2;
    }
  }
  .firstImage {
    opacity: 0;
    transition: all 1s ease;
    transition-delay: 0.2s;
    @media (max-width: 768px) {
      transform: translate(100px, 0px);
    }
    &.active {
      transform: translate(0px, 0px);
      opacity: 1;
    }
  }
  .secondImage {
    opacity: 0;
    transition: all 1s ease;
    transition-delay: 0.3s;
    @media (max-width: 768px) {
      transform: translate(100px, 0px);
    }
    &.active {
      transform: translate(0px, 0px);
      opacity: 1;
    }
  }

  @media (max-width: 1024px) and (min-width: 768px) {
    .backArrow {
      left: -10%;
    }
    .forwardArrow {
      left: 110%;
    }
  }
  @media (min-width: 1025px) {
    .backArrow {
      display: none;
    }
    .forwardArrow {
      display: none;
    }
  }
`;

export const FeatureImage = styled(animated.div)`
  display: ${(props) => (props.active ? "block" : "none")};
  position: relative;
  .lottieDesktop {
    display: none;
    @media (min-width: 1025px) {
      display: block;
      width: 500px;
      height: 550px;
    }
  }
  .desktop {
    width: ${rem(500)};
    height: ${rem(550)};
    display: block;
  }
  .mobile {
    width: ${rem(320)};
    height: ${rem(420)};
  }
`;

export const Section = styled.div`
  display: flex;
  width: 100vw;
  justify-content: center;
  align-items: center;
  scroll-snap-align: start;
`;

export const Column = styled.div`
  width: ${rem(320)};
  height: 100%;
  display: flex;
  position: relative;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  flex: 1;
  & ~ #featureSection {
    @media (max-width: 768px) {
      padding: 100px 0 0;
      flex: none;
      height: auto;
    }
  }
  .titleText {
    position: absolute;
    background-image: linear-gradient(to bottom, #2f353f, #2f343f);
    font-family: gilroyextrabold;
    font-size: ${rem(24)};
    font-weight: 800;
    font-stretch: normal;
    font-style: normal;
    line-height: 1;
    letter-spacing: -0.5px;
    text-align: left;
    background-clip: text;
    -webkit-background-clip: text;
    color: transparent;
    top: 45%;
  }
  .heroImage {
    position: absolute;
    width: ${rem(520)};
    height: 100%;
    top: 45%;
  }
  .secondaryImage {
    position: absolute;
    height: 100%;
    top: 65%;
    left: 65%;
  }

  @media (min-width: 768px) {
    justify-content: center;
    width: ${(props) => (props.width ? rem(props.width) : "auto")};
    height: auto;
    align-items: center;
    &.contentSection {
      width: 50%;
      margin: 3rem 0;
    }
    .heroImage {
      width: ${rem(550)};
      height: ${rem(550)};
      top: 50%;
      left: 50%;
      transform: translate(-100%, -50%);
    }
    .secondaryImage {
      top: 50%;
      left: 50%;
      transform: translate(-175%, 50%);
    }
    .titleText {
      top: 50%;
      left: 50%;
      transform: translate(-150%, -300%);
      font-size: ${rem(50)};
    }
  }
  @media (min-width: 1025px) {
    align-items: flex-start;
  }
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  &.hasslefreeMobile {
    margin-left: ${rem(50)};
  }
  &.content1 {
    align-items: center;
    justify-content: center;
    margin: ${rem(50)} 0;
  }
`;

export const H1 = styled.h3`
  font-family: gilroyextrabold;
  font-size: ${rem(32)};
  font-weight: 800;
  font-stretch: normal;
  font-style: normal;
  margin: ${rem(20)} 0;
  line-height: 1;
  letter-spacing: -1px;
  text-align: center;
  color: #484848;
  @media (min-width: 1025px) {
    font-size: ${rem(50, "medium")};
    text-align: left;
  }
  @media (min-width: 1440px) {
    font-size: ${rem(50)};
  }
`;

export const AnimateText = styled.h3`
  font-family: gilroyextrabold;
  font-size: ${rem(36)};
  color: #2f353f;
  font-stretch: normal;
  font-style: normal;
  margin: ${rem(20)} 0;
  line-height: 1;
  letter-spacing: -1px;
  width: 300px;
  letter-spacing: -1px;
  font-feature-settings: "liga" off;
`;

export const AnimateImage = styled.div`
  width: 500px;
  transform: translate(0px, 100px);
`;
export const AnimateImage2 = styled.div`
  width: ${(props) => (props.width ? rem(props.width) : rem(200))};
  transform: translate(0px, 100px);
  opacity: 0;
  transition: all 0.5s ease;
  transition-delay: 0.2s;
  position: absolute;
  bottom: 0;
  right: ${(props) => (props.right ? rem(props.right) : "100px")};
  &.active {
    transform: translate(0px, 0px);
    opacity: 1;
  }
`;
