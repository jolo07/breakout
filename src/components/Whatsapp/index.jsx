import React from "react";
import { FaWhatsapp } from "react-icons/fa";
import { Container } from "./style";
import Link from "next/link";
import { tracker } from "../../utils/tracker";

export default function WhatsApp() {
  return (
    <Container>
      <div
        onClick={() => {
          tracker("whatsapp_cta", { cta_from: "float button" });
        }}
      >
        <Link href="https://wa.me/919847181078/?text=Hi, I would like to know more on marketfeed.">
          <a
            style={{ color: "white" }}
            className="float"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaWhatsapp className="my-float ml-4" />
          </a>
        </Link>
      </div>
    </Container>
  );
}
