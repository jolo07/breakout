import styled, { keyframes } from "styled-components";
import { rem } from "../../utils/remConverter";

export const Container = styled.div`
  display: none;
  .float {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background-color: #25d366;
    box-shadow: 1px 3px 10px #0000002e;
    border-radius: 50px;
    bottom: 20px;
    color: #fff !important;
    cursor: pointer;
    font-size: 30px;
    height: 56px;
    right: 20px;
    position: fixed;
    text-align: center;
    width: 56px;
    z-index: 100;
    @media (min-width: 768px) {
      right: 60px;
      bottom: 60px;
    }
  }
  .compress {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background-color: #25d366;
    box-shadow: 1px 3px 10px #0000002e;
    border-radius: 50px;
    bottom: 20px;
    padding: 10px;
    color: #fff !important;
    cursor: pointer;
    font-size: 30px;
    height: 56px;
    right: 20px;
    position: fixed;
    text-align: center;
    width: 56px;
    z-index: 10;
    .chatWithUs {
      display: none;
    }
    @media (min-width: 768px) {
      right: 60px;
      bottom: 60px;
    }
  }
  .expand {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    position: fixed;
    height: 56px;
    padding: 27px 32px 27px 35px;
    width: ${rem(56)};
    bottom: 20px;
    right: 20px;
    background-color: #25d366;
    color: #fff !important;
    border-radius: 50px;
    text-align: center;
    font-size: 30px;
    cursor: pointer;
    box-shadow: 1px 3px 10px #0000002e;
    z-index: 10;
    @media (min-width: 768px) {
      right: 60px;
      bottom: 60px;
    }
  }
`;
