import Image from "next/image";
import { useEffect, useState } from "react";
import { gumletLoaderMarketfeedUniversity } from "../../utils/gumletLoader";
import { Section, Container, H3, H2, P, Column, Row } from "./style";
import { useSpring } from "react-spring";

const About = (props) => {
  const [startAnimation, setStartAnimation] = useState(false);
  const [stopAnimation, setStopAnimation] = useState(true);
  useEffect(() => {
    const featureSection = document.querySelector(".learning");
    document.addEventListener("scroll", function () {
      if (!startAnimation && featureSection !== null) {
        setStartAnimation(isInViewport(featureSection));
      }
    });
    return () =>
      document.removeEventListener("scroll", function () {
        if (!startAnimation && featureSection !== null) {
          setStartAnimation(isInViewport(featureSection));
        }
      });
  }, []);

  useEffect(() => {
    if (startAnimation && stopAnimation) {
      setStopAnimation(false);
      headingApi({
        to: { opacity: 1, transform: "translateY(0px)" },
        from: { opacity: 0, transform: "translateY(20px)" },
        config: { duration: 500 },
      });
      paraApi({
        to: { opacity: 1, transform: "translateY(0px)" },
        from: { opacity: 0, transform: "translateY(20px)" },
        config: { duration: 500 },
        delay: 100,
      });
      rightDivAnimationApi({
        to: { opacity: 1, transform: "translateY(0px)" },
        from: { opacity: 0, transform: "translateY(20px)" },
        config: { duration: 500 },
        delay: 500,
      });
    }
  }, [startAnimation]);

  const [heading, headingApi] = useSpring(() => ({
    from: { opacity: 0, transform: "translateY(20px)" },
    config: { duration: 250 },
  }));
  const [para, paraApi] = useSpring(() => ({
    from: { opacity: 0, transform: "translateY(20px)" },
  }));
  const [rightDivAnimation, rightDivAnimationApi] = useSpring(() => ({
    from: { opacity: 0, transform: "translateY(20px)" },
  }));

  const isInViewport = (element) => {
    const rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <=
        (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  };

  return (
    <Section>
      <Container>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Column width={325} className="leftDiv">
            <H3 style={heading} className="learning">
              <span style={{ color: "#637BFF" }}>Learn</span>
              <br></br>Together
            </H3>
            <P style={para}>
              Community Based <br></br>
              <span className="extrabold">Learning Programs</span> Run<br></br>{" "}
              by Experienced Traders
            </P>
          </Column>
        </div>
        <Column className="rightDiv">
          <Row style={rightDivAnimation} className="content1">
            <Image
              loader={gumletLoaderMarketfeedUniversity}
              src="/assurance.png"
              width={80}
              height={80}
              alt="assurance"
            />
            <P className="capital margin">
              Hassle-free<br></br>learning program
            </P>
          </Row>
          <Row className="content2">
            <Column style={rightDivAnimation} className="keypoints">
              <H2>5K+</H2>
              <P style={{ opacity: "0.8" }}>
                Students<br></br>Trained
              </P>
            </Column>
            <Column style={rightDivAnimation} className="keypoints">
              <H2>P&L</H2>
              <P style={{ opacity: "0.8" }}>
                Verified<br></br> Trainers
              </P>
            </Column>
            <Column style={rightDivAnimation} className="keypoints">
              <H2>80% +</H2>
              <P style={{ opacity: "0.8" }}>
                Profitable
                <br /> Traders
              </P>
            </Column>
          </Row>
        </Column>
        <Column className="rightDivMobile">
          <Row style={rightDivAnimation} className="hasslefreeMobile">
            <Image
              loader={gumletLoaderMarketfeedUniversity}
              src="/assurance.png"
              width={80}
              height={80}
              alt="assurance"
            />
            <P className="capital margin">
              Hassle-free learning<br></br> program
            </P>
          </Row>
          <Row>
            <Column style={rightDivAnimation} className="keypoints">
              <H2>5K+</H2>
              <P className="colorAndTextAlignMobile">
                Students<br></br> Trained
              </P>
            </Column>
            <Column style={rightDivAnimation} className="keypoints">
              <H2>P&L</H2>
              <P className="colorAndTextAlignMobile">
                Verified<br></br> Trainers
              </P>
            </Column>
            <Column style={rightDivAnimation} className="keypoints">
              <H2>80%</H2>
              <P className="colorAndTextAlignMobile">
                Profitable<br></br> Traders
              </P>
            </Column>
          </Row>
        </Column>
      </Container>
    </Section>
  );
};

export default About;
