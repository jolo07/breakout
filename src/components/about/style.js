import styled from "styled-components";
import { rem } from "../../utils/remConverter";
import { animated } from "react-spring";

export const Section = styled.div`
  display: flex;
  width: 100vw;
  justify-content: center;
  align-items: center;
`;

export const Container = styled.div`
  width: 100%;
  height: ${rem(928)};
  padding: ${rem(32)};
  position: relative;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  @media (min-width: 768px) {
    height: ${rem(618)};
    flex-direction: row;
    align-items: flex-end;
    justify-content: center;
  }
  @media (min-width: 1280px) {
    height: ${rem(618)};
    align-items: flex-end;
    justify-content: space-between;
    padding: 0 ${rem(150)} 0 ${rem(200)};
  }
  @media (min-width: 1440px) {
    width: ${rem(1440)};
  }
`;

export const H3 = styled(animated.h3)`
  font-family: gilroyextrabold;
  font-size: ${rem(32)};
  font-weight: 800;
  font-stretch: normal;
  font-style: normal;
  margin: ${rem(20)} 0;
  line-height: 1;
  letter-spacing: -1px;
  text-align: center;
  color: #484848;
  @media (min-width: 768px) {
    font-size: ${rem(50, "medium")};
    text-align: left;
  }
  @media (min-width: 1440px) {
    font-size: ${rem(50)};
  }
`;

export const H2 = styled(animated.h2)`
  width: ${rem(55)};
  font-family: gilroybold;
  font-size: ${rem(28)};
  font-weight: 800;
  font-stretch: normal;
  font-style: normal;
  margin: ${rem(12)} 0;
  line-height: 1;
  letter-spacing: -1px;
  text-align: left;
  color: #484848;
  @media (min-width: 768px) {
    width: ${rem(135)};
    font-size: ${rem(50, "medium")};
  }
  @media (min-width: 1440px) {
    font-size: ${rem(50)};
  }
`;

export const P = styled(animated.div)`
  font-family: gilroymedium;
  font-size: ${rem(14)};
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: normal;
  text-align: center;
  color: #484848CC;
  &.capital {
    text-transform: uppercase;
    color: #6e7176
    font-size: ${rem(12)};
  }
  &.colorAndTextAlignMobile {
    text-align: left;
  }
  &.margin {
    width: ${rem(300)};
    margin: 0 0 0 ${rem(20)};
    text-align: left;
    color: #484848;
    font-family: gilroyregular;
    line-height: 26px;
    letter-spacing: 2px;
  }
  .extrabold {
    font-family: gilroyextrabold;
  }
  @media (min-width: 768px) {
    text-align: left;
    font-size: ${rem(16)};
  }
`;

export const Column = styled(animated.div)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  &.rightDiv {
    display: none;
  }
  &.rightDivMobile {
    width: 120%;
    margin-top: ${rem(60)};
    padding: ${rem(30)} ${rem(40)};
  }
  &.keypoints {
    width: 50%;
  }
  @media (min-width: 768px) {
    width: ${(props) => (props.width ? rem(props.width) : "auto")};
    align-items: flex-start;
    &.rightDivMobile {
      display: none;
    }
    &.leftDiv {
      margin: 3rem;
    }
    &.rightDiv {
      width: ${rem(550)};
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      align-items: flex-start;
    }
  }
`;

export const Row = styled(animated.div)`
 display: flex;
 align-items: center;
 justify-content: flex-start;
 width: 100%;
 &.hasslefreeMobile {
   margin: 0 0 ${rem(24)} 0};
 }
 &.content1 {
  width: auto;
  align-items: center;
}
@media (min-width: 375px) {
  &.hasslefreeMobile {
    margin: 0 0 ${rem(24)} ${rem(50)};
  }
 }
 @media (min-width: 425px) {
  &.hasslefreeMobile {
    margin: 0 0 ${rem(24)} ${rem(75)};
  }
 }
@media (min-width: 1280px) {
  &.content1 {
    margin-bottom: ${rem(37)};
  }
  &.content2 {
    margin-bottom: ${rem(37)};
  }
}
`;
