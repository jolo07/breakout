import React, { useEffect, useState } from "react";
import {
  Container,
  H1,
  H2,
  Button,
  Tag,
  HeroImage,
  Section,
  SubBrokingWrapper,
  BottomLogoContainer,
} from "./style";
import {
  gumletLoaderMarketfeedUniversity,
  gumletLoaderBasic,
} from "../../utils/gumletLoader";
import Image from "next/image";
import { useSpring } from "react-spring";
import Icon from "../Icons";
import { tracker } from "../../utils/tracker";
import Link from "next/link";

const MarketfeedUniversity = () => {
  const [smallDevice, setSmallDevice] = useState(false);

  useEffect(() => {
    if (window.innerWidth < 768) {
      setSmallDevice(true);
    } else {
      setSmallDevice(false);
    }
  }, []);

  const onClickHandler = () => {
    const doc = document.getElementById("programsOffered");
    window.scroll({
      top: doc.offsetTop - 20,
      behavior: "smooth",
    });
    tracker("join_next_batch", { cta_from: "first section" });
  };

  const heading = useSpring({
    from: { opacity: 0.3, transform: "translateY(20px)" },
    to: { opacity: 1, transform: "translateY(0px)" },
    config: { duration: 500 },
  });
  const para = useSpring({
    from: { opacity: 0.3, transform: "translateY(20px)" },
    to: { opacity: 1, transform: "translateY(0px)" },
    config: { duration: 350 },
    delay: 150,
  });
  const buttonAnimation = useSpring({
    from: { opacity: 0, transform: "translateY(20px)" },
    to: { opacity: 1, transform: "translateY(0px)" },
    config: { duration: 200 },
    delay: 250,
  });
  const tagAnimation = useSpring({
    from: { opacity: 0, transform: "translateY(20px)" },
    to: { opacity: 1, transform: "translateY(0px)" },
    config: { duration: 500 },
    delay: 500,
  });
  const imageAnimation = useSpring({
    from: { opacity: 0.5, scale: 0.9 },
    to: { opacity: 1, scale: 1 },
    config: { duration: 1000 },
  });

  return (
    <Section>
      <Container>
        <div className="pattern">
          <Icon name="pattern" width={"102"} height={"118"} />
        </div>
        <H1 style={heading}>
          <span style={{ color: "#484848" }}>Learn Stock Market&nbsp; </span>
          <span style={{ color: "#637bff" }}>Trading, LIVE</span>
        </H1>
        <H2 style={para}>
          <span style={{ color: "rgba(72, 72, 72, 0.7)" }}>
            Learn and trade together with <br />
            India's top traders.&nbsp;
          </span>
        </H2>
        <Button onClick={() => onClickHandler()} style={buttonAnimation}>
          Join Next Batch
        </Button>
        <Tag style={tagAnimation}>Trusted by over 5000+ Traders</Tag>
        <BottomLogoContainer>
          <p>backed by</p>
          {!smallDevice ? (
            <Image
              src="/YC_Logo_dark.svg"
              loader={gumletLoaderBasic}
              height={60}
              width={290}
              alt="fundfolio y-combinator"
            />
          ) : (
            <Image
              loader={gumletLoaderBasic}
              src="/YC_Logo_dark.svg"
              height={20}
              width={92}
              alt="fundfolio y-combinator"
            />
          )}
        </BottomLogoContainer>
        {smallDevice ? (
          <Image
            loader={gumletLoaderMarketfeedUniversity}
            src="/mobile_hero_image.png"
            layout={"fill"}
            objectFit="contain"
            objectPosition="bottom"
            alt="learn stock market trading"
          />
        ) : (
          <HeroImage style={imageAnimation}>
            <Image
              loader={gumletLoaderMarketfeedUniversity}
              src="/hero_img_web.png"
              width={870}
              height={646}
              objectFit="contain"
              alt={"learn stock market trading"}
            />
          </HeroImage>
        )}
        <SubBrokingWrapper>
          New to Trading?
          <Link href={"https://marketfeed.app/open-demat-account"}>
            <a
              target={"_blank"}
              className="sub-broking"
              rel="noreferrer noopener"
            >
              Open Demat Account
            </a>
          </Link>
        </SubBrokingWrapper>
      </Container>
    </Section>
  );
};

export default MarketfeedUniversity;
