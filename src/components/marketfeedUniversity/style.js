import styled from "styled-components";
import { rem } from "../../utils/remConverter";
import { animated } from "react-spring";

export const Section = styled.div`
  display: flex;
  width: 100vw;
  justify-content: center;
  align-items: center;
  background: linear-gradient(151.68deg, #d9f9ff -2.69%, #c6cbff 98.31%);
`;

export const Container = styled.div`
  width: 100vw;
  height: ${rem(850)};
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  flex-grow: 0;
  padding: ${rem(120)} ${rem(36)} ${rem(36)};
  .pattern {
    display: none;
  }
  @media (min-width: 768px) {
    height: ${rem(1100)};
  }
  @media (min-width: 1024px) {
    height: ${rem(1164)};
  }
  @media (min-width: 1280px) {
    height: ${rem(768)};
    align-items: flex-start;
    justify-content: center;
    padding: ${rem(162)} ${rem(147)};
    .pattern {
      display: block;
      position: absolute;
      top: ${rem(153)};
      left: ${rem(104)};
    }
  }
  @media (min-width: 1980px) {
    max-height: ${rem(850)};
    width: ${rem(1440)};
    padding: 11.125rem 11rem;
    .pattern {
      display: block;
      position: absolute;
      top: ${rem(183)};
      left: ${rem(124)};
    }
  }
`;

export const SubBrokingWrapper = styled.div`
  width: 100vw;
  height: ${rem(53)};
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  bottom: -${rem(53)};
  background-color: #f4fefb;
  font-size: 14px;
  color: #484848;
  font-family: gilroymedium;
  z-index: 1;

  a {
    font-size: 14px;
    text-decorations: none;
    color: #14cc60;
    letter-spacing: -0.5px;
    font-family: gilroybold;
    margin-left: 6px;
  }
  @media (min-width: 768px) {
    display: none;
  }
`;

export const H1 = styled(animated.h1)`
  width: ${rem(220)};
  font-family: gilroyextrabold;
  margin: ${rem(12)} 0;
  font-size: ${rem(32)};
  font-weight: 800;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: normal;
  text-align: center;
  text-transform: none;
  color: #ffffff;
  @media (min-width: 360px) {
    width: ${rem(220, "small")};
    margin: ${rem(12, "small")} 0;
    font-size: ${rem(32, "small")};
  }
  @media (min-width: 768px) {
    width: ${rem(495)};
    margin: ${rem(12, "mobile")}, 0;
    font-size: ${rem(50, "medium")};
  }
  @media (min-width: 1024px) {
    margin: ${rem(12)}, 0;
  }
  @media (min-width: 1280px) {
    text-align: left;
  }
`;

export const H2 = styled(animated.div)`
  flex-grow: 0;
  margin-bottom: ${rem(24)};
  font-family: gilroymedium;
  font-size: ${rem(14)};
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: center;
  color: #484848;
  @media (min-width: 360px) {
    margin-bottom: ${rem(24, "small")};
    font-size: ${rem(14, "small")};
  }
  @media (min-width: 768px) {
    width: ${rem(280, "medium")};
    margin-bottom: ${rem(24, "mobile")};
    font-size: ${rem(16)};
  }
  @media (min-width: 1280px) {
    width: ${rem(280)};
    text-align: left;
    margin-bottom: ${rem(36, "medium")};
  }
`;

export const Button = styled(animated.div)`
  width: ${rem(127)};
  height: ${rem(40)};
  z-index: 1;
  flex-grow: 0;
  display: flex;
  margin-bottom: ${rem(24)};
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  color: #ffffff;
  font-size: 12px;
  font-family: gilroybold;
  text-transform: none;
  box-shadow: 0 4px 18px 0 rgba(0, 0, 0, 0.09);
  border: solid 1px rgba(255, 255, 255, 0.1);
  background-color: #14cc60;
  cursor: pointer;
  overflow: hidden;
  position: relative;
  -webkit-transition: all 0.5s;
  transition: all 0.5s;
  &:before {
    content: "";
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    -webkit-transition: none;
    transition: none;
  }
  &:hover:before {
    width: 120%;
    background-color: rgba(255, 255, 255, 0);
    -webkit-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
  }
  @media (min-width: 768px) {
    width: ${rem(197)};
    height: ${rem(54)};
    margin-bottom: ${rem(30)};
    font-size: 18px;
  }
  @media (min-width: 1280px) {
    border-radius: 16px;
    font-size: ${rem(18, "medium")};
  }
  @media (min-width: 1440px) {
    font-size: ${rem(18)};
  }
`;

export const Tag = styled(animated.div)`
  font-size: ${rem(12)};
  font-weight: bold;
  font-stretch: normal;
  font-family: gilroybold;
  font-style: normal;
  line-height: normal;
  letter-spacing: 1px;
  text-align: center;
  color: rgb(72, 72, 72, 0.5);
  @media (min-width: 360px) {
    font-size: ${rem(12, "small")};
  }
  @media (min-width: 768px) {
    font-size: ${rem(12, "mobile")};
  }
  @media (min-width: 1280px) {
    font-size: ${rem(16, "medium")};
  }
  @media (min-width: 1440px) {
    font-size: ${rem(16)};
  }
`;

export const HeroImage = styled(animated.div)`
  position: absolute;
  right: -10px;
  bottom: -10px;
`;

export const BottomLogoContainer = styled.div`
  margin-top: ${rem(10)};
  p{
    color: rgba(72, 72, 72, 0.7);
    font-size: ${rem(12)}
    padding-top :10px;
    opacity:60%;
    padding-bottom :6px;
  }
  img{
    width: ${rem(140)};
  }
`;
