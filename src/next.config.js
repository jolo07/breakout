module.exports = {
  distDir: "../.next",
  optimizeFonts: true,
  env: {
    NEXT_PUBLIC_FIREBASE_API_KEY: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
    NEXT_PUBLIC_AUTH_DOMAIN: process.env.NEXT_PUBLIC_AUTH_DOMAIN,
    NEXT_PUBLIC_DATA_BASE_URL: process.env.NEXT_PUBLIC_DATA_BASE_URL,
    NEXT_PUBLIC_PROJECT_ID: process.env.NEXT_PUBLIC_PROJECT_ID,
    NEXT_PUBLIC_STORAGE_BUCKET: process.env.NEXT_PUBLIC_STORAGE_BUCKET,
    NEXT_PUBLIC_MESSAGING_SENDER_ID:
      process.env.NEXT_PUBLIC_MESSAGING_SENDER_ID,
    NEXT_PUBLIC_FIREBASE_APP_ID: process.env.NEXT_PUBLIC_FIREBASE_APP_ID,
    NEXT_PUBLIC_FIREBASE_MEASUREMENT_ID:
      process.env.NEXT_PUBLIC_FIREBASE_MEASUREMENT_ID,
  },
  async rewrites() {
    return [
      {
        source: "/learn",
        destination: "https://kb-stage.mystagingwebsite.com/docs/",
      },
      {
        source: "/learn/category/:path*",
        destination:
          "https://kb-stage.mystagingwebsite.com/docs-category/:path*",
      },
      {
        source: "/learn/:path*",
        destination: "https://kb-stage.mystagingwebsite.com/docs/:path*",
      },
    ];
  },
};
