import Head from "next/head";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { useRouter } from "next/router";
import Theme from "../constants/theme";
import "./font.css";
const GlobalStyle = createGlobalStyle`
  *{
    box-sizing: border-box;
    ::-webkit-scrollbar {
      width: 0;
      background: transparent;
    }
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    -moz-tap-highlight-color: rgba(0, 0, 0, 0);
    scroll-behavior: smooth;
  }
  body, html {
    margin: 0;
    padding: 0;
    font-size: 100%;
    box-sizing: border-box;
    width: 100vw;
    background-color: #ffffff;
    overflow-x: hidden;
    -webkit-overflow-scrolling: touch;
    font-family: gilroyregular;
    font-variant-ligatures: no-common-ligatures;
    .grecaptcha-badge { 
      visibility: hidden;
    }
    
  }
  #__next{
    margin: 0;
    height: 100%;
    width: 100vw;
  }
  h1,h2,h3,h4,p,span{
    margin: 0;
    font-display:swap;
  }
  a {
    text-decoration: none;
  }
  :root {
    --PhoneInput-color--focus: #000;
    --PhoneInputInternationalIconPhone-opacity: 0.8;
    --PhoneInputInternationalIconGlobe-opacity: 0.65;
    --PhoneInputCountrySelect-marginRight: 0.35em;
    --PhoneInputCountrySelectArrow-width: 0.3em;
    --PhoneInputCountrySelectArrow-marginLeft: var(--PhoneInputCountrySelect-marginRight);
    --PhoneInputCountrySelectArrow-borderWidth: 1px;
    --PhoneInputCountrySelectArrow-opacity: 0.45;
    --PhoneInputCountrySelectArrow-color: inherit;
    --PhoneInputCountrySelectArrow-color--focus: var(--PhoneInput-color--focus);
    --PhoneInputCountrySelectArrow-transform: rotate(45deg);
    --PhoneInputCountryFlag-aspectRatio: 1.5;
    --PhoneInputCountryFlag-height: 1em;
    --PhoneInputCountryFlag-borderWidth: 1px;
    --PhoneInputCountryFlag-borderColor: rgba(0,0,0,0.5);
    --PhoneInputCountryFlag-borderColor--focus: var(--PhoneInput-color--focus);
    --PhoneInputCountryFlag-backgroundColor--loading: rgba(0,0,0,0.1);
  }
  
  .PhoneInput {
    /* This is done to stretch the contents of this component. */
    display: flex;
    align-items: center;
  }
  
  .PhoneInputInput {
    /* The phone number input stretches to fill all empty space */
    flex: 1;
    /* The phone number input should shrink
       to make room for the extension input */
    min-width: 0;
  }
  
  .PhoneInputCountryIcon {
    width: calc(var(--PhoneInputCountryFlag-height) * var(--PhoneInputCountryFlag-aspectRatio));
    height: var(--PhoneInputCountryFlag-height);
  }
  
  .PhoneInputCountryIcon--square {
    width: var(--PhoneInputCountryFlag-height);
  }
  
  .PhoneInputCountryIcon--border {
      box-shadow: 0 0 0 var(--PhoneInputCountryFlag-borderWidth) var(--PhoneInputCountryFlag-borderColor),
      inset 0 0 0 var(--PhoneInputCountryFlag-borderWidth) var(--PhoneInputCountryFlag-borderColor);
  }
  
  .PhoneInputCountryIconImg {
    display: block;
    width: 100%;
    height: 100%;
  }
  
  .PhoneInputInternationalIconPhone {
    opacity: var(--PhoneInputInternationalIconPhone-opacity);
  }
  
  .PhoneInputInternationalIconGlobe {
    opacity: var(--PhoneInputInternationalIconGlobe-opacity);
  }
  
  
  .PhoneInputCountry {
    position: relative;
    align-self: stretch;
    display: flex;
    align-items: center;
    margin-right: var(--PhoneInputCountrySelect-marginRight);
  }
  
  .PhoneInputCountrySelect {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
    border: 0;
    opacity: 0;
    cursor: pointer;
  }
  
  .PhoneInputCountrySelect[disabled] {
    cursor: default;
  }
  
  .PhoneInputCountrySelectArrow {
      display: block;
      content: '';
      width: var(--PhoneInputCountrySelectArrow-width);
      height: var(--PhoneInputCountrySelectArrow-width);
      margin-left: var(--PhoneInputCountrySelectArrow-marginLeft);
      border-style: solid;
      border-color: var(--PhoneInputCountrySelectArrow-color);
      border-top-width: 0;
      border-bottom-width: var(--PhoneInputCountrySelectArrow-borderWidth);
      border-left-width: 0;
      border-right-width: var(--PhoneInputCountrySelectArrow-borderWidth);
      transform: var(--PhoneInputCountrySelectArrow-transform);
      opacity: var(--PhoneInputCountrySelectArrow-opacity);
    }
  
    .PhoneInputCountrySelect:focus + .PhoneInputCountryIcon + .PhoneInputCountrySelectArrow {
      opacity: 1;
      color: var(--PhoneInputCountrySelectArrow-color--focus);
    }
  
    .PhoneInputCountrySelect:focus + .PhoneInputCountryIcon--border {
      box-shadow: 0 0 0 var(--PhoneInputCountryFlag-borderWidth) var(--PhoneInputCountryFlag-borderColor--focus),
        inset 0 0 0 var(--PhoneInputCountryFlag-borderWidth) var(--PhoneInputCountryFlag-borderColor--focus);
    }
  
    .PhoneInputCountrySelect:focus + .PhoneInputCountryIcon .PhoneInputInternationalIconGlobe {
      opacity: 1;
      color: var(--PhoneInputCountrySelectArrow-color--focus);
    }
  `;

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  return (
    <ThemeProvider theme={Theme}>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
        />
        <meta
          name="description"
          content="Become a successful stock market trader and investor, from beginner to expert, learn directly from Sharique Samsudheen and his team of stock market experts!"
        />
        <meta
          name="keywords"
          content="Online stock trading courses, stockmarket courses, Stock market trading, Stock market trading courses, Stock trading courses, stock market, Sharique Samsudheen,
          Learn how to trade in markets, invest in stocks, learn stock market"
        />
        <meta name="author" content="Sharique Samsudheen" />
        <title>
          Learn Stock Market Trading | Stock Market Basics for Beginners -
          marketfeed
        </title>
        <link rel="canonical" href="https://marketfeed.com"/>
        <link
          rel="marketfeed icon"
          href="https://fundfolio.gumlet.io/fundfolio/marketfeed/marketfeedFavicon.png"
        />
        <script
          async
          src={`https://www.googletagmanager.com/gtag/js?id=G-687Q2MNF0P`}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-687Q2MNF0P', {
              page_path: window.location.pathname,
            });
          `,
          }}
        />
        <script
          defer
          dangerouslySetInnerHTML={{
            __html: `var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode: "48324a462f231c734944407688c2fb1b3be2bb1e02060f4d825745f1cbb543b9a81d6b547a35c109951d24f6be71d2d0", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.in/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");`,
          }}
        />
        <script async src="https://www.google-analytics.com/analytics.js" />
        <script
          defer
          dangerouslySetInnerHTML={{
            __html: `
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', 547909253124158);
              `,
          }}
        />
        <noscript>
          <img
            height="1"
            width="1"
            style={{ display: "none" }}
            src={`https://www.facebook.com/tr?id=547909253124158&ev=PageView&noscript=1`}
          />
        </noscript>
      </Head>
      <GlobalStyle />
      <Component {...pageProps} />
      <script async src="/scripts/pure-swipe.js" />
    </ThemeProvider>
  );
}
export default MyApp;
