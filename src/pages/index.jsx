import React, { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import App from "../components/App";
import MarketfeedUniversity from "../components/marketfeedUniversity";
const Faq = dynamic(() => import("../components/Faq"));
const ProgramsOffered = dynamic(() => import("../components/ProgramsOffered"));
const TalkToUs = dynamic(() => import("../components/TalkToUs/index.jsx"));
import OfferModal from "../components/OfferModal";
const LanguageModel = dynamic(() => import("../components/LanguageModel"));
import { Loader } from "../components/Loader";
const TradeTogether = dynamic(() => import("../components/TradeTogether"));
import { tracker } from "../utils/tracker";
import Whatsapp from "../components/Whatsapp";

export default function Home() {
  const [isLoading, setLoading] = useState(true);
  const [isModalVisible, setModalVisible] = useState(false);
  const [isLanguageModalVisible, setIsLanguageModalVisible] = useState(false);
  const [programSelected, setProgramSelected] = useState("");
  const [client, setClient] = useState(false);

  useEffect(() => {
    setClient(true);
  }, []);

  useEffect(() => {
    document.body.style.overflow = "hidden";
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  const handlePageNavigation = (program) => {
    setProgramSelected(program);
    tracker("join_next_batch", { cta_from: program });
    if (
      program === "basicsPremium" ||
      program === "swing" ||
      program === "options"
    )
      setIsLanguageModalVisible(true);
    else
      window.location.href =
        "https://breakout.fundfolio.in/product/breakout-basics/";
  };
  return (
    <App>
      {isLoading ? <Loader /> : null}
      <OfferModal
        visible={isModalVisible}
        close={() => setModalVisible(false)}
      />
      <LanguageModel
        programSelected={programSelected}
        visible={isLanguageModalVisible}
        close={() => setIsLanguageModalVisible(false)}
      />
      <MarketfeedUniversity />
      {client ? <TradeTogether /> : null}
      <ProgramsOffered
        openOfferModal={() => {
          tracker("check_eligibility", { cta_from: "basics_premium" });
          setModalVisible(true);
        }}
        showLanguage={(program) => handlePageNavigation(program)}
      />
      <Faq />
      <TalkToUs />
      <Whatsapp />
    </App>
  );
}
