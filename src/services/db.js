import { delBasePath } from "next/dist/next-server/lib/router/router";
import fire from "../utils/fire";
export const examplefirebasefunction = (value) => {
  //db calls
};

export const doesUserDetailsCollectionExist = async (id) => {
  const getUserDetails = await fire
    .firestore()
    .collection("users")
    .doc(id)
    .get();
  const userData = {
    ...getUserDetails.data(),
    exists: getUserDetails.exists,
  };
  return userData;
};

export const addUserRecord = async (id, userRecord) => {
  try {
    const createUserRecordDoc = fire.firestore().collection("users").doc(id);
    await createUserRecordDoc.set(userRecord);
    return Promise.resolve(true);
  } catch (error) {
    throw Promise.reject(false);
  }
};
