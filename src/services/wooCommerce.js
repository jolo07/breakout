import WooCommerceRestApi from "@woocommerce/woocommerce-rest-api";

const getAllPrograms = async () => {
  const WooCommerce = new WooCommerceRestApi({
    url: "https://payments.marketfeed.com",
    consumerKey: "ck_5d3cc9a1af30f18c21e2769c7d25db10f36431cf",
    consumerSecret: "cs_6672646752e8154effcb21fbc9751204de651176",
    version: "wc/v3",
    queryStringAuth: true,
    axiosConfig: {
      headers: {},
    },
  });

  try {
    const response = await WooCommerce.get(`products`);
    const data = await response.data;
    return Promise.resolve(data);
  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};

const getProgramData = async (productId, variantId) => {
  const WooCommerce = new WooCommerceRestApi({
    url: "https://payments.marketfeed.com",
    consumerKey: "ck_5d3cc9a1af30f18c21e2769c7d25db10f36431cf",
    consumerSecret: "cs_6672646752e8154effcb21fbc9751204de651176",
    version: "wc/v3",
    queryStringAuth: true,
    axiosConfig: {
      headers: {},
    },
  });

  try {
    const response = await WooCommerce.get(`products/${productId}/variations/${variantId}`);
    const data = await response.data;
    return Promise.resolve(data);
  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};

export { getProgramData, getAllPrograms };
