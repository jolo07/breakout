import md5 from "md5";
const createProfileImage = (email) => {
    const lowerCaseEmail = email.toLowerCase()
    return `http://www.gravatar.com/avatar/${md5(lowerCaseEmail)}?d=identicon`;
};
export default createProfileImage;
