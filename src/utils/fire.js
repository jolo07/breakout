import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY.toString(),
  authDomain: process.env.NEXT_PUBLIC_AUTH_DOMAIN.toString(),
  databaseURL: process.env.NEXT_PUBLIC_DATA_BASE_URL.toString(),
  projectId: process.env.NEXT_PUBLIC_PROJECT_ID.toString(),
  storageBucket: process.env.NEXT_PUBLIC_STORAGE_BUCKET.toString(),
  messagingSenderId: process.env.NEXT_PUBLIC_MESSAGING_SENDER_ID.toString(),
  appId: process.env.NEXT_PUBLIC_FIREBASE_APP_ID.toString(),
  measurementId: process.env.NEXT_PUBLIC_FIREBASE_MEASUREMENT_ID.toString(),
};

export default !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app();

export const loginWithEmail = (values, setSubmitting, setErrorMessage) => {
  firebase
    .auth()
    .signInWithEmailAndPassword(values.email, values.password)
    .then((userCredential) => {
      // Signed in
      var user = userCredential.user;
      setSubmitting(false);
      // ...
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      setErrorMessage(errorMessage);
      setSubmitting(false);
      // ..
    });
};
