export const roundOffToNearestFifty = (num) => {
  return Math.round(num / 50) * 50;
};
