// log specific events happening.
export const tracker = async (action, params) => {
  window.gtag("event", action, params);
};
